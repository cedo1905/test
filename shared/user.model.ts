export interface UserModel {
  id: string; // ovo polje mora biti unique
  role: 'admin' | 'master'; // prava i privilegija (hardcodovati admina za sad)
  blocked: boolean; //blokiran
  isActive: boolean; // aktivan
  fullName: string; // ime i prezime
  email: string; // ovo polje mora biti unique
  password?: string; // lozinka
}
