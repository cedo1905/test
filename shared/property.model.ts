import { UserModel } from './user.model';

export interface PropertyModel {
  _id: string; // ovo polje mora biti unique
  property_id: number;
  creator: UserModel; // admin koji je kreirao nekretninu
  offerType: 'rent' | 'sale'; // izdavanje ili prodaja
  city: string; // grad
  district: string; // dio grada
  address: string; // adresa
  longitude: string; // geografska duzina (maps)
  latitude: string; // geografska duzina (maps)
  locationDescription: string; // opis lokacije riječima
  residenceType: 'apartment' | 'house' | 'office' | 'land'; // tip nekretnine
  rooms: 'studio' | '1' | '2' | '3' | '4' | '5' | 'other'; // broj soba
  price: number; // cijena (za izdavanje ide po mjesecu / za prodaju ukupno)
  area: number; // kvadratura
  floor: string; // sprat
  toilet: number; // toalet
  elevator: boolean; // lift
  furnished: boolean; // opremljen
  terrace: boolean; // terasa
  privateParking: boolean; // privatni parking
  internet: string; // internet
  owner: string; // vlasnik
  attic: boolean; // potkrovlje
  photos: string[];
  gallery360: string; // ovo jos nije skroz definisano
  createdAt: Date; // datum kreiranja
  modifiedAt: Date; // datum izmjene
  isActive: boolean; //aktivna nekretnina
  ownerFullName: string; //Ime i prezime korisnika koji je ponudio nekretninu
  ownerEmail: string; //Email korisnika koji je ponudio nekretninu
  ownerPhoneNumber: string; // Broj Telefona korisnika koji je ponudio nekretninu
}
