import express = require('express');

const Router = express.Router();
const advertisingController = require('./../controllers/advertisingController');
const authController = require('./../controllers/authController');

Router.route('/send').post(advertisingController.addAdvertising);

Router.route('/').get(authController.protect, advertisingController.showAdvertising);

Router.route('/:id').delete(authController.protect, advertisingController.deleteAdvertising);

module.exports = Router;
