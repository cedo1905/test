const express = require('express');
const mediaController = require('./../controllers/mediaController');
const authController = require('./../controllers/authController');
const Router = express.Router();
Router.use(authController.protect);
Router.route('/').get(mediaController.showMedia);

Router.route('/').post(
  authController.protect,
  mediaController.uploadPropertyPhoto,
  mediaController.addMedia
);

Router.route('/:photoId').delete(authController.protect, mediaController.deleteFile);
module.exports = Router;
