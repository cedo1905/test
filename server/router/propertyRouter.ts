import express = require('express');
import { property } from 'lodash';

const propertyController = require('./../controllers/propertyController');
const authController = require('./../controllers/authController');
const contactController = require('./../controllers/contactController');

const cacheController = require('./../controllers/cacheController');

const Router = express.Router();

Router.route('/').get(
  authController.protect,
  propertyController.showProperty,
  cacheController.checkCache
);

Router.route('/add').post(authController.protect, propertyController.addProperty);

Router.route('/rent').get(propertyController.rent, cacheController.checkCacheRent);

Router.route('/sale').get(propertyController.sale, cacheController.checkCacheSale);

Router.route('/nonactive').get(propertyController.nonActive);

Router.route('/maprent').get(cacheController.checkCacheMapRent);

Router.route('/mapsale').get(cacheController.checkCacheMapSale);

Router.route('/contact').post(contactController.contactUs);

Router.route('/sendproperty/:propertyId').post(propertyController.sendFavoriteEmail);

Router.route('/:id/search').get(propertyController.findPropertySearch);

Router.route('/:propertyID/singledata').get(propertyController.findPropertyData);

Router.route('/:propertyID/adminsearch').get(
  authController.protect,
  propertyController.searchProperty
);

Router.route('/:propertyID').get(authController.protect, propertyController.findForEdit);

Router.route('/:id').delete(authController.protect, propertyController.deleteProperty);

Router.route('/:id').put(authController.protect, propertyController.updateProperty);

Router.route('/:id/:type').put(propertyController.blockUnblockProperty);

module.exports = Router;
