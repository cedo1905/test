import express = require('express');
const userController = require('./../controllers/userController');
const authController = require('./../controllers/authController');
const propertyController = require('./../controllers/propertyController');

const Router = express.Router();

Router.delete('/:id', authController.protect, userController.deleteUser);
Router.put('/:id', authController.protect, userController.updateUser);
Router.put('/:id/password', authController.protect, userController.updateUserPassword);
Router.put('/:id/:type', authController.protect, userController.blockUser);
Router.route('/').get(authController.protect, userController.showUser);

Router.route('/:id').get(authController.protect, userController.showAdminById);

module.exports = Router;
