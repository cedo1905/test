import express = require('express');

const Router = express.Router();
const scheduleController = require('./../controllers/scheduleController');
const authController = require('./../controllers/authController');

Router.route('/send').post(scheduleController.createSchedule);

Router.route('/').get(authController.protect, scheduleController.seeSchedule);

Router.route('/:id').delete(authController.protect, scheduleController.deleteSchedule);
module.exports = Router;
