import express = require('express');
import mongoose = require('mongoose');
import dotenv = require('dotenv');
dotenv.config({ path: './config.env' });
import cors = require('cors');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');

const app = express();
console.log(process.env.REDIS_PORT);
app.use(cors());
app.use(express.json());
app.use(helmet());

app.use(mongoSanitize());
app.use(xss());

app.use(express.static(`./server/public`));

const userRouter = require('./router/userRouter');
const propertyRouter = require('./router/propertyRouter');
const authRouter = require('./router/authRouter');
const mediaRouter = require('./router/mediaRouter');
const schedulingRouter = require('./router/schedulingRouter');
const advertisingRouter = require('./router/advertisingRouter');

app.use('/api/auth', authRouter);
app.use('/api/user', userRouter);
app.use('/api/property', propertyRouter);
app.use('/api/media', mediaRouter);
app.use('/api/scheduling', schedulingRouter);
app.use('/api/advertising', advertisingRouter);

const port = process.env.PORT || 3000;

const DB = process.env.DATABASE.replace('<PASSWORD>', process.env.DATABASE_PASSWORD);

mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then((con) => {
    console.log('You are successfuly connected on your DATABASE');
  });

app.listen(port, () => {
  console.log(`Successfuly connected on port ${port}`);
});
