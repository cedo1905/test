const AWS = require('aws-sdk');

exports.awsConfig = () => {
  AWS.config.update({
    aws_access_key_id: process.env.AWS_ACCESS_KEY_ID,
    accessKeyId: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION,
  });

  AWS.config.update({});
};
