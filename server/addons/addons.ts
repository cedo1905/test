import { totalmem } from 'os';

exports.error = (
  message: any,
  statusCode: any,
  res: {
    status: (
      arg0: any
    ) => {
      (): any;
      new (): any;
      json: { (arg0: { status: string; message: any }): void; new (): any };
    };
  }
) => {
  res.status(statusCode).json({
    status: 'Error',
    message: message,
  });
};

exports.successCreate = (model, name, statusCode, res, total?) => {
  const response = {
    status: 'Success',
    total: total.length,
    data: {},
  };
  if (name) {
    response.data[name] = model;
  }

  res.status(statusCode).json(response);
};

exports.success = (model, name, statusCode, res) => {
  const response = {
    status: 'Success',
    data: {},
  };
  if (name) {
    response.data[name] = model;
  }

  res.status(statusCode).json(response);
};

exports.successResponse = (model, name, total, statusCode, res) => {
  const response = {
    status: 'Success',
    data: {
      total: total,
    },
  };
  if (name) {
    response.data[name] = model;
  }

  res.status(statusCode).json(response);
};

exports.successUpdate = (model: any, statusCode, res, name?) => {
  const response = {
    status: 'Success',
    data: {},
  };
  if (name) {
    response.data[name] = model;
  } else {
    delete response.data;
  }

  res.status(statusCode).json(response);
};

exports.successDelete = (statusCode, res) => {
  res.status(statusCode).json({
    status: 'Success',
  });
};

exports.filter = (req, query) => {
  if (req.query.priceFrom) {
    query = query.find({ price: { $gte: req.query.priceFrom } });
  }
  if (req.query.priceTo) {
    query = query.find({ price: { $lte: req.query.priceTo } });
  }
  if (req.query.district) {
    query = query.find({ district: req.query.district });
  }
  if (req.query.residenceType) {
    const type = req.query.residenceType.split(',');
    const type2 = req.query.residenceType.split(',').join(' ');

    query = query.find({ residenceType: { $in: type } });
  }
  if (req.query.city) {
    if (req.query.city == 'all') {
      query = query.find();
    } else {
      query = query.find({ city: req.query.city });
    }
  }
  if (req.query.rooms) {
    const rooms = req.query.rooms.replace('+', ' ').split(',');
    console.log(rooms);
    query = query.find({ rooms: { $in: rooms } });
  }

  if (req.query.sort) {
    const sortBy = req.query.sort.split(',').join(' ');
    console.log(sortBy);
    query = query.sort(sortBy);
  } else {
    query = query.sort('-createdAt');
  }
  const skip = req.query.skip * 1;
  const take = req.query.take * 1;
};
