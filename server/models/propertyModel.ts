import mongoose = require('mongoose');
const shortid = require('shortid');
const slugify = require('slugify');
const AutoIncrement = require('mongoose-sequence')(mongoose);

shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');
export interface Iproperty extends mongoose.Document {
  address: string;
  district: string;
  city: string;
  slug: string;
}
const propertySchema = new mongoose.Schema({
  creator: [
    {
      name: {
        type: String,
      },
      email: {
        type: String,
      },
      _id: {
        type: String,
      },
    },
  ],

  offerType: {
    type: String,
    enum: ['rent', 'sale'],
    required: true,
  },

  city: {
    type: String,
    required: true,
  },

  district: {
    type: String,
  },

  address: {
    type: String,
    required: true,
  },

  longitude: {
    type: String,
    default: 'point',
    required: true,
  },

  latitude: {
    type: String,
    default: 'point',
    required: true,
  },

  locationDescription: {
    type: String,
  },

  residenceType: {
    type: String,
    enum: ['apartment', 'house', 'shop', 'office', 'land'],
    required: true,
  },

  rooms: {
    type: String,
    enum: ['studio', '1', '2', '3', '4', '5', 'other'],
    required: true,
  },

  price: {
    type: Number,
    default: '0',
    required: true,
  },

  area: {
    type: Number,
    default: '0',
  },

  floor: {
    type: String,

    default: '0',
  },

  toilet: {
    type: String,

    default: '0',
  },

  elevator: {
    type: Boolean,

    default: false,
  },

  furnished: {
    type: Boolean,

    default: false,
  },

  terrace: {
    type: Boolean,

    default: false,
  },

  privateParking: {
    type: Boolean,

    default: false,
  },

  internet: {
    type: String,
  },

  owner: {
    type: String,
  },

  attic: {
    type: Boolean,

    default: false,
  },

  photos: [
    {
      type: String,
      default: 'photo.jpg',
    },
  ],

  createdAt: {
    type: Date,
    default: Date.now,
  },

  slug: {
    type: String,
  },

  gallery360: {
    type: String,
  },
  ownerFullName: {
    type: String,
  },
  ownerEmail: {
    type: String,
  },
  ownerPhoneNumber: {
    type: String,
  },

  isActive: {
    type: Boolean,
    default: true,
  },
});
propertySchema.plugin(AutoIncrement, { inc_field: 'property_id' });

propertySchema.index({ offerType: 1 });
propertySchema.index({ price: 1 });
propertySchema.index({ residenceType: 1 });
propertySchema.index({ district: 1 });
propertySchema.index({ rooms: 1 });

propertySchema.pre<Iproperty>('save', function (next) {
  const slug = `${this.address} ${this.district}  ${this.city}`;
  this.slug = slugify(slug, { lower: true });
  next();
});

propertySchema.post(/^updateOne/, async function () {
  const find = await this.findOne();
  console.log(find);
  const slug = `${this.address} ${this.district} ${this.name}`;
  find.slug = `${slug}`;
  await find.save();
});

const Property = mongoose.model('property', propertySchema);

module.exports = Property;
