import mongoose = require('mongoose');
import validator = require('validator');
const advertisingSchema = new mongoose.Schema({
  name: {
    type: String,
  },

  surname: {
    type: String,
  },

  email: {
    type: String,
    validate: [validator.isEmail, 'Please write valide mail address'],
  },

  phoneNumber: {
    type: String,
  },

  type: {
    type: String,
    enum: ['rent', 'sale'],
  },

  address: {
    type: String,
  },

  city: {
    type: String,
  },

  district: {
    type: String,
  },

  longitude: {
    type: String,
  },

  latitude: {
    type: String,
  },

  measuringPlotting: {
    type: Boolean,
    default: false,
  },
  description: {
    type: String,
  },

  plottingBasics: {
    type: Boolean,
    default: false,
  },

  geodesy: {
    type: Boolean,
    default: false,
  },

  underConstruction: {
    type: Boolean,
    default: false,
  },
});

const Advertising = mongoose.model('advertising', advertisingSchema);

module.exports = Advertising;
