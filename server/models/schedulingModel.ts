import mongoose = require('mongoose');
import validator = require('validator');
const schedulingModel = new mongoose.Schema({
  firstName: {
    type: String,
  },

  lastName: {
    type: String,
  },

  email: {
    type: String,
    validate: [validator.isEmail, 'Please write valide mail address'],
  },

  phone: {
    type: String,
  },

  realEstateID: {
    type: String,
  },
});

const Schedule = mongoose.model('scheduling', schedulingModel);

module.exports = Schedule;
