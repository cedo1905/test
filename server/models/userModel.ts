import mongoose = require('mongoose');
import bcrypt = require('bcryptjs');
import validator = require('validator');

export interface IUser extends mongoose.Document {
  password: string;
  passwordConfirm: string;
}

const userSchema = new mongoose.Schema({
  role: {
    type: String,
    enum: ['master', 'admin'],
    default: 'admin',
  },

  isActive: {
    type: Boolean,
    default: 'true',
  },

  fullName: {
    type: String,
    trim: true,
    required: [true, 'Please insert your name'],
  },

  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: [true, 'please insert your email adress'],
    validate: [validator.isEmail, 'Please write valide email adress'],
  },

  password: {
    type: String,
    required: [true, 'Password is required'],
    select: false,
    minlength: [6, 'Password must container more then 6 character'],
  },
  passwordConfirm: {
    type: String,
    required: [true, 'Please confirm your password'],
    minlength: [6, 'Password must container more then 6 character'],
    validate: {
      validator: function (el) {
        return el === this.password;
      },
      message: 'Please write correct password',
    },
  },
});

userSchema.pre<IUser>('save', async function (next) {
  this.password = await bcrypt.hash(this.password, 12);
  this.passwordConfirm = undefined;

  next();
});

userSchema.methods.correctPassword = async function (candidatePassword, userPassword) {
  return await bcrypt.compare(candidatePassword, userPassword);
};
const User = mongoose.model('user', userSchema);

module.exports = User;
