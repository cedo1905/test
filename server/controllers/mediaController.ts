import { Query } from 'mongoose';
import { SSL_OP_MICROSOFT_BIG_SSLV3_BUFFER, SSL_OP_NO_SSLv3 } from 'constants';

const multer = require('multer');
const sharp = require('sharp');
const fs = require('fs');
const AWS = require('aws-sdk');
const multerS3 = require('multer-s3');
const s3Storage = require('multer-s3-transform');
const Property = require('./../models/propertyModel');
const Media = require('./../models/mediaModel');
const addons = require('./../addons/addons');
const config = require('./../addons/config');

config.awsConfig();

AWS.config.update({});

let s3 = new AWS.S3();

const multerStorage = s3Storage({
  s3: s3,
  acl: 'public-read',
  bucket: 'nest360',
  contentType: multerS3.AUTO_CONTENT_TYPE,
  key: function (req, file, cb) {
    cb(null, `user-${req.user.id}-${Date.now()}.jpeg`),
      cb(null, (file.filename = `user-${req.user.id}-${Date.now()}.jpeg`));
  },
  shouldTransform: function (req, file, cb) {
    cb(null, /^image/i.test(file.mimetype));
  },
  transforms: [
    {
      id: 'Original',
      key: function (req, file, cb) {
        cb(null, file.filename);
      },
      transform: function (req, file, cb) {
        cb(null, sharp().jpeg());
      },
    },

    {
      id: 'small',
      key: function (req, file, cb) {
        cb(null, `sm-${file.filename}`);
      },
      transform: function (req, file, cb) {
        cb(null, sharp().resize(400, 250).jpeg());
      },
    },

    {
      id: 'medium',
      key: function (req, file, cb) {
        cb(null, `md-${file.filename}`);
      },
      transform: function (req, file, cb) {
        cb(
          null,
          sharp()
            .resize(1000, 500)
            .composite([
              {
                input: 'server/public/upload/logo2.png',
                gravity: 'center',
              },
            ])
            .jpeg()
        );
      },
    },

    {
      id: 'Large',
      key: function (req, file, cb) {
        cb(null, `lg-${file.filename}`);
      },
      transform: function (req, file, cb) {
        cb(
          null,
          sharp()
            .resize({
              height: 1200,
            })
            .composite([
              {
                input: 'server/public/upload/logo2.png',
              },
            ])
            .jpeg()
        );
      },
    },
  ],
});

const upload2 = multer({
  storage: multerStorage,
});
exports.uploadPropertyPhoto = upload2.single('photo');

exports.addMedia = async (req, res) => {
  try {
    const url = `https://nest360.s3.amazonaws.com/${req.file.filename}`;
    const addMedia = await Media.create({
      photo: url,
    });
    addons.success(addMedia, 'media', 200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.showMedia = async (req, res) => {
  try {
    const queryObj = { ...req.query };
    const excLudedFields = ['skip', 'sort', 'take', 'fields'];
    excLudedFields.forEach((el) => delete queryObj[el]);

    let query = Media.find(queryObj);

    const skip = req.query.skip * 1;
    const take = req.query.take * 1;

    const media = await query.skip(skip).limit(take).sort('-_id');
    const total = await Media.find().countDocuments();

    addons.successResponse(media, 'media', total, 200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.deleteFile = async (req, res) => {
  try {
    const query = await Media.findOneAndDelete({ _id: req.params.photoId });

    const url = new URL(query.photo);
    const key = url.pathname.slice(1);
    const propertyPhoto = await Property.updateMany(
      {},
      {
        $pull: {
          photos: `${url.href}`,
        },
      }
    );
    const params = {
      Bucket: 'nest360',
      Delete: {
        Objects: [
          {
            Key: `${key}`,
          },

          {
            Key: `sm-${key}`,
          },

          {
            Key: `md-${key}`,
          },

          {
            Key: `lg-${key}`,
          },
        ],
        Quiet: false,
      },
    };

    const s3Delete = await s3.deleteObjects(params).promise();
    addons.successDelete(200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};
