const Advertising = require('./../models/advertisingModel');
addons = require('./../addons/addons');
sendEmail = require('./email');

exports.addAdvertising = async (req, res) => {
  try {
    const addAdvertising = await Advertising.create({
      name: req.body.name,
      surname: req.body.surname,
      email: req.body.email,
      phoneNumber: req.body.phoneNumber,
      type: req.body.type,
      city: req.body.city,
      address: req.body.address,
      district: req.body.district,
      longitude: req.body.longitude,
      latitude: req.body.latitude,
      package: req.body.package,
      description: req.body.description,
      measuringPlotting: req.body.measuringPlotting,
      plottingBasics: req.body.plottingBasics,
      geodesy: req.body.geodesy,
      underConstruction: req.body.underConstruction,
    });

    let translate;
    let subject;
    if (req.body.type === 'sale') {
      translate = 'Prodaja';
      subject = 'Zahtjev za oglasavanje nekretnine - prodaja';
    } else if (req.body.type === 'rent') {
      translate = 'Izdavanje';
      subject = 'Zahtjev za oglasavanje nekretnine - izdavanje';
    }

    let measuringPlotting;
    let da = 'DA';
    let ne = 'NE';

    if (req.body.measuringPlotting === true) {
      measuringPlotting = `Mjerenje i iscrtavanje osnove nekretnine: ${da}`;
    } else {
      measuringPlotting = `Mjerenje i iscrtavanje osnove nekretnine: ${ne}`;
    }

    let plottingBasics;
    if (req.body.plottingBasics === true) {
      plottingBasics = `Geodetsko snimanje zemljista. (Plaća se avansno): ${da}`;
    } else {
      plottingBasics = `Geodetsko snimanje zemljista. (Plaća se avansno): ${ne}`;
    }

    let geodesy;
    if (req.body.geodesy === true) {
      geodesy = `Geodetsko snimanje zemljista: ${da}`;
    } else {
      geodesy = `Geodetsko snimanje zemljista: ${ne}`;
    }

    let underConstruction;
    if (req.body.underConstruction === true) {
      underConstruction = `Nekretnina je u fazi izgradnje: ${da}`;
    } else {
      underConstruction = `Nekretnina je u fazi izgradnje: ${ne}`;
    }

    let location = `<a href='https://www.google.com/maps?q=${req.body.latitude},${req.body.longitude}'>https://www.google.com/maps?q=${req.body.latitude},${req.body.longitude}</a>`;
    const html = `<p>Ime: ${req.body.name}<br>Prezime: ${req.body.surname}<br>Email: ${req.body.email}<br>Broj telefona: ${req.body.phoneNumber}<br>Prodaja / Izdavanje: ${translate}<br>Grad: ${req.body.city}<br>Dio grada: ${req.body.district}<br>Adresa : ${req.body.address}<br>Paket : ${req.body.package}<br>Lokacija:${location}<br>${measuringPlotting}<br>${plottingBasics}<br>${geodesy}<br>${underConstruction}<br>Opis: ${req.body.description}`;

    await sendEmail({
      from: 'nekretnine@nest360.me',
      to: `klijent@nest360.me`,
      email: req.body.email,
      subject: `${subject}`,
      html: html,
    });
    addons.success(addAdvertising, 'advertising', 200, res);

    if (sendEmail) {
      // console.log('Mail has been send');
    }
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.showAdvertising = async (req, res) => {
  try {
    const queryObj = { ...req.query };
    const addonsFields = ['skip', 'take'];
    addonsFields.forEach((addons) => delete queryObj[addons]);
    const query = Advertising.find(queryObj);

    const skip = req.query.skip * 1;
    const take = req.query.take * 1;

    const advertising = await query.skip(skip).limit(take);
    const total = await Advertising.find().countDocuments();
    addons.successResponse(advertising, 'advertising', total, 200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.deleteAdvertising = async (req, res) => {
  try {
    await Advertising.findByIdAndDelete(req.params.id);
    addons.successDelete(200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};
