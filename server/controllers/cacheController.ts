import { nextTick } from 'process';

const Property = require('./../models/propertyModel');
const redis = require('redis');

const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_URL);
client.auth(process.env.REDIS_PASSWORD, function (err) {
  if (err) throw err;
});
client.on('connect', function () {
  console.log('Connected to Redis');
});

exports.redisUpdate = async (
  redisModelProperty,
  redisModelRent,
  redisModelSale,
  redisModelMapRent,
  redisModelMapSale
) => {
  const property = await Property.find();
  client.setex(redisModelProperty, 172800, JSON.stringify(property));
  252000;
  const rent = await Property.find({ offerType: 'rent', isActive: { $in: true } })
    .select('-ownerFullName -ownerEmail -ownerPhoneNumber')
    .sort('-createdAt');
  client.setex(redisModelRent, 172800, JSON.stringify(rent));

  const sale = await Property.find({ offerType: 'sale', isActive: { $in: true } })
    .select('-ownerFullName -ownerEmail -ownerPhoneNumber')
    .sort('-createdAt');
  client.setex(redisModelSale, 172800, JSON.stringify(sale));

  const getMapRent = await Property.find({ offerType: 'rent', isActive: true })
    .select('longitude latitude property_id')
    .select('-_id');
  client.setex(redisModelMapRent, 172800, JSON.stringify(getMapRent));

  const getMapSale = await Property.find({ offerType: 'sale', isActive: true })
    .select('longitude latitude property_id')
    .select('-_id');
  client.setex(redisModelMapSale, 172800, JSON.stringify(getMapSale));
};

exports.checkCache = async (req, res, next) => {
  const data = client.get('property', (err, data) => {
    if (err) {
      addons.error(err.message, 404, res);
    }
    if (data != null) {
      res.status(200).json({
        status: 'Success',
        data: {
          total: JSON.parse(data).length,
          property: JSON.parse(data),
        },
      });
    } else {
      next();
    }
  });
};

exports.checkCacheMapRent = async (req, res, next) => {
  client.get('property_map_rent', (err, data) => {
    if (err) {
      addons.error(err.message, 404, res);
    }
    if (data != null) {
      res.status(200).json({
        status: 'Success',
        data: {
          langLat: JSON.parse(data),
        },
      });
    }
  });
};

exports.checkCacheMapSale = async (req, res, next) => {
  client.get('property_map_sale', (err, data) => {
    if (err) {
      addons.error(err.message, 404, res);
    }
    if (data != null) {
      res.status(200).json({
        status: 'Success',
        data: {
          langLat: JSON.parse(data),
        },
      });
    }
  });
};

exports.checkCacheRent = async (req, res, next) => {
  client.get('property_rent', (err, data) => {
    if (err) {
      addons.error(err.message, 404, res);
    }
    if (data != null) {
      res.status(200).json({
        status: 'Success',
        data: {
          total: JSON.parse(data).length,
          rent: JSON.parse(data),
        },
      });
    } else {
      next();
    }
  });
};

exports.checkCacheSale = async (req, res) => {
  client.get('property_sale', (err, data) => {
    if (err) {
      addons.error(err.message, 404, res);
    }
    if (data != null) {
      const total = JSON.parse(data).length;
      res.status(200).json({
        status: 'Success',
        data: {
          total,
          sale: JSON.parse(data),
        },
      });
    }
  });
};
