import { property } from 'lodash';
import { Query } from 'mongoose';
import { cwd } from 'process';

const Property = require('./../models/propertyModel');
const Media = require('./../models/mediaModel');

const cache = require('./cacheController');
const addons = require('./../addons/addons');
const config = require('./../addons/config');

const AWS = require('aws-sdk');

config.awsConfig();

let s3 = new AWS.S3();

exports.addProperty = async (req, res, next) => {
  try {
    const addProperty = await Property.create({
      creator: [{ name: req.user.fullName, email: req.user.email, _id: req.user.id }],
      offerType: req.body.offerType,
      city: req.body.city,
      district: req.body.district,
      address: req.body.address,
      longitude: req.body.longitude,
      latitude: req.body.latitude,
      locationDescription: req.body.locationDescription,
      residenceType: req.body.residenceType,
      rooms: req.body.rooms,
      price: req.body.price,
      area: req.body.area,
      floor: req.body.floor,
      toilet: req.body.toilet,
      elevator: req.body.elevator,
      furnished: req.body.furnished,
      terrace: req.body.terrace,
      privateParking: req.body.privateParking,
      internet: req.body.internet,
      owner: req.body.owner,
      attic: req.body.attic,
      createdAt: req.body.createdAt,
      photos: req.body.photos,
      gallery360: req.body.gallery360,
      ownerFullName: req.body.ownerFullName,
      ownerEmail: req.body.ownerEmail,
      ownerPhoneNumber: req.body.ownerPhoneNumber,
      isActive: req.body.isActive,
    });

    cache.redisUpdate(
      'property',
      'property_rent',
      'property_sale',
      'property_map_rent',
      'property_map_sale'
    );

    addons.success(addProperty, 'property', 200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.showProperty = async (req, res, next) => {
  try {
    cache.redisUpdate(
      'property',
      'property_rent',
      'property_sale',
      'property_map_rent',
      'property_map_sale'
    );
    const queryObj = { ...req.query };
    const addonsFields = [
      'skip',
      'sort',
      'take',
      'fields',
      'priceFrom',
      'priceTo',
      'district',
      'residenceType',
      'city',
      'rooms',
      'active',
    ];
    if (!req.originalUrl.startsWith('/api/property?')) {
      next();
    } else {
      addonsFields.forEach((addons) => delete queryObj[addons]);
      let query = Property.find(queryObj);

      addons.filter(req, query);

      if (req.query.active == 'true') {
        query = query.find({ isActive: true });
      } else if (req.query.active == 'false') {
        query = query.find({ isActive: false });
      }

      const skip = req.query.skip * 1;
      const take = req.query.take * 1;

      const total = await query;
      const property = await query.skip(skip).limit(take);
      addons.successResponse(property, 'property', total.length, 200, res);
    }
  } catch (err) {
    addons.error(err.messge, 400, res);
  }
};

exports.nonActive = async (req, res) => {
  try {
    const nonActive = await Property.find({ isActive: false });
    res.status(200).json({
      status: 'Success',
      nonActive,
    });
  } catch (err) {
    addons.error(err.message, 400, res);
  }
};

exports.blockUnblockProperty = async (req, res) => {
  try {
    if (req.params.type === 'active') {
      const blockUnblock = await Property.updateOne(
        { property_id: req.params.id },
        {
          isActive: true,
        }
      );
    } else if (req.params.type === 'inactive') {
      const blockUnblock = await Property.updateOne(
        { property_id: req.params.id },
        {
          isActive: false,
        }
      );
    } else {
      addons.error('Wrong', 404, res);
    }

    res.status(200).json({
      status: 'Success',
    });
  } catch (err) {
    addons.error(err.message, 400, res);
  }
};

exports.findPropertySearch = async (req, res, next) => {
  try {
    const property = await Property.findOne({ property_id: req.params.id }).select(
      '-ownerEmail -ownerFullName -ownerPhoneNumber'
    );
    if (property === null || property.isActive === false) {
      return res.status(200).json({ status: 'Error' });
    }
    addons.success(property, 'property', 200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.findPropertyData = async (req, res, next) => {
  try {
    const property = await Property.findOne({ property_id: req.params.propertyID }).select(
      '-ownerEmail -ownerFullName -ownerPhoneNumber'
    );
    if (property === null || property.isActive === false) {
      return res.status(404).json({ status: 'Error' });
    }
    addons.success(property, 'property', 200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

//SEARCH za ADMINA!
exports.searchProperty = async (req, res, next) => {
  try {
    const property = await Property.findOne({ property_id: req.params.propertyID });
    if (property === null) {
      return addons.error('Property with that ID does not excists , please try again', 404, res);
    }
    addons.success(property, 'property', 200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

//FIND ZA ADMINA (Uredi)!
exports.findForEdit = async (req, res, next) => {
  try {
    const property = await Property.findOne({ property_id: req.params.propertyID });
    if (property === null) {
      return addons.error('Property with that ID does not excists , please try again', 404, res);
    } else {
      addons.success(property, 'property', 200, res);
    }
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.deleteProperty = async (req, res) => {
  try {
    const test = await Property.findOne({ property_id: req.params.id });
    let photo = test.photos;
    //console.log(photo);

    //const url = new URL(test.photos);
    //console.log(url);
    // const key = url.pathname.slice(1);
    // console.log(key);
    //console.log(url);

    //let photo = url.href;
    //console.log(url.href);

    if (test.creator[0]._id == req.user._id || req.user.role === 'master') {
      const s3deletePhoto = async (key) => {
        const params = {
          Bucket: 'nest360',
          Delete: {
            Objects: [
              {
                Key: `${key}`,
              },

              {
                Key: `sm-${key}`,
              },

              {
                Key: `md-${key}`,
              },

              {
                Key: `lg-${key}`,
              },
            ],
            Quiet: false,
          },
        };
        const a = await s3.deleteObjects(params).promise();
        // console.log(a);
      };

      // console.log('asd');

      photo.forEach(async (element) => {
        /* let path = new URL(element);
        let key = path.pathname.slice(1);
        console.log(key);*/
        let link = `https://nest360.s3.amazonaws.com/${element}`;
        console.log(link);
        const find = await Media.deleteMany({
          photo: { $in: link },
        });
        console.log(find);
      });
      /*
      const a = f.split(',');
      console.log(a);
      console.log(a);
      const find = await Media.deleteMany({
        photo: { $in: a },
      });
      console.log(find);
      */
      const deleteProperty = await Property.deleteOne({ property_id: req.params.id });

      cache.redisUpdate(
        'property',
        'property_rent',
        'property_sale',
        'property_map_rent',
        'property_map_sale'
      );

      addons.successDelete(200, res);
    } else {
      return addons.error(
        'You are not creator of this product , please change only product that you made',
        401,
        res
      );
    }
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.updateProperty = async (req, res) => {
  try {
    const test = await Property.findOne({ property_id: req.params.id });
    if (test.creator[0]._id == req.user._id || req.user.role === 'master') {
      const updateProperty = await Property.updateOne({ property_id: req.params.id }, req.body, {
        new: true,
        runValidators: true,
      });

      const property = await Property.findOne({ property_id: req.params.id });

      cache.redisUpdate(
        'property',
        'property_rent',
        'property_sale',
        'property_map_rent',
        'property_map_sale'
      );

      addons.successUpdate(property, 200, res, 'property');
    } else {
      return addons.error(
        'You are not creator of this product , please change only product that you made',
        401,
        res
      );
    }
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.rent = async (req, res, next) => {
  try {
    cache.redisUpdate(
      'property',
      'property_rent',
      'property_sale',
      'property_map_rent',
      'property_map_sale'
    );

    const queryObj = { ...req.query };
    const addonsFields = [
      'skip',
      'sort',
      'take',
      'fields',
      'priceFrom',
      'priceTo',
      'district',
      'residenceType',
      'city',
      'rooms',
    ];
    addonsFields.forEach((addons) => delete queryObj[addons]);

    let query = Property.find({ offerType: 'rent', isActive: { $in: true } }, queryObj).select(
      '-ownerFullName,-ownerEmail,-ownerPhoneNumber'
    );

    if (!req.originalUrl.startsWith('/api/property/rent?')) {
      next();
    } else {
      addons.filter(req, query);

      const skip = req.query.skip * 1;
      const take = req.query.take * 1;

      const total = await query;
      const rent = await query.skip(skip).limit(take);

      addons.successResponse(rent, 'rent', total.length, 200, res);
    }
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.sale = async (req, res, next) => {
  try {
    cache.redisUpdate(
      'property',
      'property_rent',
      'property_sale',
      'property_map_rent',
      'property_map_sale'
    );

    const queryObj = { ...req.query };
    const addonsFields = [
      'skip',
      'sort',
      'take',
      'fields',
      'priceFrom',
      'priceTo',
      'district',
      'residenceType',
      'city',
      'rooms',
    ];

    addonsFields.forEach((addons) => delete queryObj[addons]);

    let query = Property.find({ offerType: 'sale', isActive: { $in: true } }, queryObj).select(
      '-ownerFullName,-ownerEmail,-ownerPhoneNumber'
    );

    if (!req.originalUrl.startsWith('/api/property/sale?')) {
      next();
    } else {
      addons.filter(req, query);

      const skip = req.query.skip * 1;
      const take = req.query.take * 1;

      const total = await query;
      const sale = await query.skip(skip).limit(take);

      res.status(200).json({
        status: 'Success',
        data: {
          total: total.length,
          sale,
        },
      });
    }
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.sendFavoriteEmail = async (req, res) => {
  try {
    const findProperty = await Property.findOne({ property_id: req.params.propertyId });
    let location;
    if (findProperty.offerType === 'sale') {
      location = `https://www.nest360.me/kupujem/nekretnine/${findProperty.property_id}-${findProperty.slug}`;
      console.log(location);
    } else if (findProperty.offerType === 'rent') {
      location = `https://www.nest360.me/iznajmljujem/nekretnine/${findProperty.property_id}-${findProperty.slug}`;
    }
    const html = `Link nekretnine : <a href='${location}'>${location}</a>`;

    await sendEmail({
      from: 'nekretnine@nest360.me',
      to: req.body.email,
      //email: req.body.email,
      subject: `nest360.me - pregled nekretnine - ${findProperty.property_id}`,
      html: html,
    });

    res.status(200).json({
      status: 'Success',
      email: req.body.email,
    });
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};
