const Schedule = require('./../models/schedulingModel');
const Property = require('./../models/propertyModel');
addons = require('./../addons/addons');
sendEmail = require('./email');

exports.createSchedule = async (req, res) => {
  try {
    const schedule = await Schedule.create({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      phone: req.body.phone,
      realEstateID: req.body.realEstateID,
    });
    const findProperty = await Property.findOne({ property_id: req.body.realEstateID });
    let location;
    if (findProperty.offerType === 'sale') {
      location = `<a href='https://www.nest360.me/kupujem/nekretnine/${findProperty.property_id}-${findProperty.slug}'>https://www.nest360.me/kupujem/nekretnine/${findProperty.property_id}-${findProperty.slug}</a>`;
    } else if (findProperty.offerType === 'rent') {
      location = `<a href='https://www.nest360.me/iznajmljujem/nekretnine/${findProperty.property_id}-${findProperty.slug}'>https://www.nest360.me/iznajmljujem/nekretnine/${findProperty.property_id}-${findProperty.slug}</a>`;
    }
    const html = `<p>Ime: ${req.body.firstName}<br>Prezime: ${req.body.lastName}<br>Email: ${req.body.email}<br>Phone: ${req.body.phone}<br>Id: ${req.body.realEstateID}<br>Link : ${location}`;
    await sendEmail({
      from: 'nekretnine@nest360.me',
      to: 'office@nest360.me',
      subject: 'Zakazivanje',
      html: html,
    });

    if (sendEmail) {
      console.log('Email has been sent');
    }

    res.status(200).json({
      status: 'Success',
      data: {
        schedule,
      },
    });
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.seeSchedule = async (req, res) => {
  try {
    const queryObj = { ...req.query };
    const addonsFields = ['skip', 'take'];
    addonsFields.forEach((addons) => delete queryObj[addons]);
    const query = Schedule.find(queryObj);

    const skip = req.query.skip * 1;
    const take = req.query.take * 1;

    const seeSchedule = await query.skip(skip).limit(take);
    const total = await Schedule.find().countDocuments();
    addons.successResponse(seeSchedule, 'schedule', total, 200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.deleteSchedule = async (req, res) => {
  try {
    await Schedule.findByIdAndDelete(req.params.id);
    addons.successDelete(200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};
