addons = require('./../addons/addons');
sendEmail = require('./email');

exports.contactUs = async (req, res) => {
  try {
    const html = `Ime i Prezime: ${req.body.fullName}<br>Broj Telefona: ${req.body.phoneNumber}<br>Email: ${req.body.email}<br>Poruka:${req.body.message}`;

    await sendEmail({
      from: 'nekretnine@nest360.me',
      to: `office@nest360.me`,
      email: req.body.email,
      subject: 'Kontakt',
      html: html,
    });
    res.status(200).json({
      status: 'Success',
    });
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};
