const nodemailer = require('nodemailer');
let sendEmail = async (option) => {
  const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    auth: {
      user: process.env.EMAIL_USERNAME,
      pass: process.env.EMAIL_PASSWORD,
    },
  });

  const mailOption = {
    from: option.from,
    to: option.to,
    subject: option.subject,
    text: option.message,
    html: option.html,
  };

  await transporter.sendMail(mailOption);
};

module.exports = sendEmail;
