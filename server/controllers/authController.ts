import { raw } from 'express';
import { userInfo } from 'os';

const User = require('./../models/userModel');
const addons = require('./../addons/addons');
const jwt = require('jsonwebtoken');

exports.register = async (
  req: { body: { fullName: any; email: any; password: any; passwordConfirm: any; role: any } },
  res: {
    status: (
      arg0: number
    ) => {
      (): any;
      new (): any;
      json: { (arg0: { status: string; addUser?: any; message?: any }): void; new (): any };
    };
  }
) => {
  try {
    const addUser = await User.create({
      fullName: req.body.fullName,
      email: req.body.email,
      password: req.body.password,
      passwordConfirm: req.body.passwordConfirm,
      role: req.body.role,
    });

    addons.success(addUser, 'user', 200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.protect = async (
  req: { headers: { authorization: string }; user: any },
  res: any,
  next: () => void
) => {
  try {
    let token: any;
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
      token = req.headers.authorization.split(' ')[1];
    }

    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer null')) {
      return addons.error('Please log in so you can get access to this page', 401, res);
    }

    if (!token) {
      return addons.error('Please log in so you can get access to this page', 401, res);
    }

    const verifyUser = jwt.verify(token, process.env.JWT_SECRET, {
      expires: new Date(Date.now() + process.env.JWT_EXPIRES_IN),
    });
    const findUser = await User.findById({ _id: verifyUser.id });

    if (findUser.blocked === true)
      return addons.error(
        'You dont have permission to visit this page.Please contact your MASTER',
        401,
        res
      );

    req.user = findUser;

    next();
  } catch (err) {
    addons.error(err.stack, 404, res);
  }
};

exports.logIn = async (req, res) => {
  try {
    const { email, password } = req.body;

    if (!email || !password) {
      return addons.error('Please write both email and password', 401, res);
    }

    const user = await User.findOne({ email }).select('password');
    if (!user || !(await user.correctPassword(password, user.password))) {
      return addons.error('Email or password are not correct,please try again', 401, res);
    }
    const findUser = await User.findById({ _id: user.id });

    if (findUser.isActive == false)
      return addons.error('You dont have permission to Login please contact your MASTER', 401, res);
    const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET);

    res.status(200).json({
      token,
      findUser,
    });
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};
