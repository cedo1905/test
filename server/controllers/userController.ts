let User = require('./../models/userModel');
let addons = require('./../addons/addons');

exports.showUser = async (req, res) => {
  try {
    const queryObj = { ...req.query };
    const excLudedFields = ['skip', 'sort', 'take'];
    excLudedFields.forEach((el) => delete queryObj[el]);

    let query = User.find(queryObj);

    const skip = req.query.skip * 1 || 1;
    const take = req.query.take * 1 || 100;

    query = query.skip(skip).limit(take);
    const users = await query;
    const total = await User.find().countDocuments();

    addons.successResponse(users, 'users', total, 200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.showAdminById = async (req, res) => {
  try {
    const showUser = await User.findById(req.params.id);

    addons.success(showUser, 'user', 200, res);
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.deleteUser = async (req, res) => {
  try {
    if (req.user.role === 'master') {
      const removeUser = await User.findByIdAndRemove({ _id: req.params.id });

      addons.successDelete(200, res);
    } else {
      addons.error('You dont have permission to do this action', 401, res);
    }
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.updateUser = async (req, res) => {
  try {
    let user = await User.findById({ _id: req.params.id });
    if (req.user.id == user._id || req.user.role == 'master') {
      await User.findByIdAndUpdate(req.params.id, req.body);
      const showUser = await User.findById(req.params.id);

      addons.success(showUser, 'user', 200, res);
    } else {
      return addons.error('You dont have permission to do this action', 401, res);
    }
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.updateUserPassword = async (req, res) => {
  try {
    let user = await User.findById({ _id: req.params.id });

    if (req.user.id == user._id || req.user.role == 'master') {
      const { password, passwordConfirm } = req.body;

      if (password == passwordConfirm) {
        user.password = req.body.password;
        user.passwordConfirm = req.body.passworConfirm;
        await user.save({ validateBeforeSave: false });
      } else {
        addons.error('Wrong password', 404, res);
      }

      addons.success(user, 'user', 200, res);
    } else {
      return addons.error('You dont have permission to do this action', 401, res);
    }
  } catch (err) {
    addons.error(err.message, 404, res);
  }
};

exports.blockUser = async (req, res) => {
  try {
    if (req.user.role === 'master' && req.params.type == 'block') {
      const block = await User.findByIdAndUpdate(req.params.id, {
        isActive: false,
      });

      res.status(200).json({
        status: 'Success',
      });
    } else if (req.user.role === 'master' && req.params.type == 'unblock') {
      const block = await User.findByIdAndUpdate(req.params.id, {
        isActive: true,
      });

      res.status(200).json({
        status: 'Success',
      });
    } else {
      return addons.error('Wrong address', 404, res);
    }
  } catch (err) {
    addons.error(err.stack, 404, res);
  }
};
