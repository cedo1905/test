# Nest360 app server / admin panel

This project uses the [MEAN stack](<https://en.wikipedia.org/wiki/MEAN_(software_bundle)>):

- [**M**ongoose.js](http://www.mongoosejs.com) ([MongoDB](https://www.mongodb.com)): database
- [**E**xpress.js](http://expressjs.com): backend framework
- [**A**ngular 2+](https://angular.io): frontend framework
- [**N**ode.js](https://nodejs.org): runtime environment

Other tools and technologies used:

- [Angular CLI](https://cli.angular.io): frontend scaffolding
- [Bcrypt.js](https://github.com/dcodeIO/bcrypt.js): password encryption

Whole stack is in TypeScript, from frontend to backend, giving you the advantage to code in one single language throughout the all stack.

## Prerequisites

1. Install [Node.js](https://nodejs.org) and [MongoDB](https://www.mongodb.com)
2. Install Angular CLI: `npm i -g @angular/cli`
3. From project root folder install all the dependencies: `npm i`

## Run

### Development mode

1. `npm run dev`: [concurrently](https://github.com/kimmobrunfeldt/concurrently) execute MongoDB, Angular build, TypeScript compiler and Express server.
2. `npm run server`: execute MongoDB, Angular build, TypeScript compiler and Express server. (without Angular)

A window will automatically open at [localhost:4200](http://localhost:4200). Angular and Express files are being watched. Any change automatically creates a new bundle, restart Express server and reload your browser.

### Production mode

`npm run prod`: run the project with a production bundle and AOT compilation listening at [localhost:3000](http://localhost:3000)
