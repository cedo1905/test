import { NgModule } from '@angular/core';

import { VerticalLayout1Module } from 'app/layout/vertical/layout-1/layout-1.module';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [VerticalLayout1Module, MatButtonModule],
  exports: [VerticalLayout1Module],
  declarations: [],
})
export class LayoutModule {}
