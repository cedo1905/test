import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { catchError, tap } from 'rxjs/operators';
import { throwError, BehaviorSubject } from 'rxjs';
import { UserLogged } from '../userLogged.model';
import { Router } from '@angular/router';

export interface AuthResponseData {
  findUser: {
    email: string;
    fullName: string;
    isActive: boolean;
    role: string;
    _id: string;
    _token: string;
  };
  token: string;
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  user = new BehaviorSubject<UserLogged | undefined>(undefined);

  constructor(private http: HttpClient, private router: Router) {}
  userLogin(email: string, password: string) {
    return this.http
      .post<AuthResponseData>(`${environment.serverUrl}/auth/login`, {
        email,
        password,
      })
      .pipe(
        catchError(this.handleError),
        tap((response) => {
          this.handleAuth(response.token, response.findUser);
        })
      );
  }

  userLogout() {
    // return this.http.post(`${environment.url}/auth/logout`);
  }

  private handleAuth(
    token: string,
    user: {
      email: string;
      fullName: string;
      isActive: boolean;
      role: string;
      _id: string;
      _token: string;
    }
  ) {
    const newUser = new UserLogged(
      user.email,
      user.fullName,
      user.isActive,
      user.role,
      user._id,
      token
    );

    this.user.next(newUser);
    localStorage.setItem('userData', JSON.stringify(newUser));
  }

  private handleError(errorMsg: HttpErrorResponse) {
    let errorMessage = 'An unknown error has occured!';
    if (!errorMsg.error) {
      return throwError(errorMessage);
    }
    errorMessage = errorMsg.error.message;
    return throwError(errorMessage);
  }

  autoLogin() {
    const loadUser: {
      email: string;
      fullName: string;
      isActive: boolean;
      role: string;
      _id: string;
      _token: string;
    } = JSON.parse(localStorage.getItem('userData'));
    if (!loadUser) {
      return;
    } else {
      const newLoadedUser = new UserLogged(
        loadUser.email,
        loadUser.fullName,
        loadUser.isActive,
        loadUser.role,
        loadUser._id,
        loadUser._token
      );
      if (loadUser._token) {
        this.user.next(newLoadedUser);
      }
    }
  }

  logout() {
    this.user.next(null);
    this.router.navigate(['/login']);
    localStorage.removeItem('userData');
  }
}
