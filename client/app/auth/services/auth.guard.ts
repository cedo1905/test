import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private _fuseNavigationService: FuseNavigationService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    router: RouterStateSnapshot
  ): boolean | UrlTree | Promise<boolean | UrlTree> | Observable<boolean | UrlTree> {
    return this.authService.user.pipe(
      map((user) => {
        if (user || user?.isActive === true) {
          if (user.role !== 'admin') {
            this.hideMenuItem(false);
          } else {
            this.hideMenuItem(true);
          }
          return true;
        }
        return this.router.createUrlTree(['/login']);
      })
    );
  }
  private hideMenuItem(hideVal: boolean) {
    this._fuseNavigationService.updateNavigationItem('admin-panel', {
      hidden: hideVal,
    });
  }
}
