export class UserLogged {
  constructor(
    public email: string,
    public fullName: string,
    private _isActive: boolean,
    public role: string,
    private _id: string,
    private _token?: string
  ) {}

  get token() {
    return this._token;
  }

  get isActive() {
    return this._isActive;
  }
  get id() {
    return this._id;
  }
}
