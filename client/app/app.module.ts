import { HttpClientModule } from '@angular/common/http';
import { NgModule, LOCALE_ID } from '@angular/core';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';

import { FuseProgressBarModule, FuseSidebarModule } from '@fuse/components';
import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { RealEstateModule } from './main/real-estate/real-estate.module';
import { AuthModule } from 'app/auth/auth.module';
import { AdminPanelModule } from './main/admin-panel/admin-panel.module';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from '@angular/flex-layout';

//Import language data
import localeSrLatnMe from '@angular/common/locales/sr-Latn-ME';
import { registerLocaleData } from '@angular/common';
import { SharedModule } from './shared/shared.module';


registerLocaleData(localeSrLatnMe);

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,

    TranslateModule.forRoot(),

    // Material moment date module
    MatMomentDateModule,

    // Material
    MatButtonModule,
    MatIconModule,

    // Fuse modules
    FuseModule.forRoot(fuseConfig),
    FuseProgressBarModule,
    FuseSharedModule,
    FuseSidebarModule,
    FuseSharedModule,

    // App modules
    LayoutModule,
    RealEstateModule,
    AdminPanelModule,
    AuthModule,
    CoreModule,
    SharedModule,
  ],
  bootstrap: [AppComponent],
  providers: [{ provide: LOCALE_ID, useValue: 'sr-Latn-ME' }],
})
export class AppModule { }
