import { NgModule } from '@angular/core';
import { DialogBoxModule } from './dialog-box/dialog-box.module';
import { ImageUploadModule } from './image-upload/image-upload.module';
import { ImageUploadDialogModule } from './image-upload-dialog/image-upload-dialog.module';
import { AreaPipe } from './pipes/area.pipe';
import { RentPipe } from './pipes/rent.pipe';
import { TranslatePipe } from './pipes/translate.pipe';
import { ShortenPipe } from './pipes/shorten.pipe';
import { AddressCustomPipe } from './pipes/address.pipe';

@NgModule({
  declarations: [AreaPipe, RentPipe, TranslatePipe, AddressCustomPipe, ShortenPipe],
  imports: [DialogBoxModule, ImageUploadModule, ImageUploadDialogModule],
  exports: [
    DialogBoxModule,
    ImageUploadModule,
    ImageUploadDialogModule,
    AreaPipe,
    RentPipe,
    TranslatePipe,
    AddressCustomPipe,
    ShortenPipe,
  ],
})
export class SharedModule {}
