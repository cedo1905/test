import { Component, OnInit, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.scss'],
})
export class DialogBoxComponent implements OnInit {
  message: string;
  image: string;
  environment: any;

  constructor(private dialogRef: MatDialogRef<DialogBoxComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.message = data.message;
    if (data.image) {
      this.image = data.image;
    }
  }

  ngOnInit(): void {
    this.environment = environment;
  }

  onYesClick() {
    this.dialogRef.close(true);
  }

  onNoClick() {
    this.dialogRef.close();
  }
}
