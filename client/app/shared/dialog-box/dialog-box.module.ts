import { NgModule } from '@angular/core';
import { DialogBoxComponent } from './dialog-box.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [DialogBoxComponent],
  imports: [CommonModule, MatButtonModule],
  exports: [],
})
export class DialogBoxModule {}
