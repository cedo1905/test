import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'address',
})
export class AddressCustomPipe implements PipeTransform {
  transform(address: string[], joinBy: string) {
    let newAddress = address
      .filter((val) => {
        if (val !== ' ' || !val) {
          return val;
        }
      })
      .join(joinBy);

    return newAddress;
  }
}
