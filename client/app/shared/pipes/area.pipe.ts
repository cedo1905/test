import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'area',
})
export class AreaPipe implements PipeTransform {
  transform(area: string) {
    return `${area} <var>m<sup>2</sup></var>`;
  }
}
