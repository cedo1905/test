import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rent',
})
export class RentPipe implements PipeTransform {
  transform(price: string, offerType?: string) {
    if (offerType === 'rent' || offerType === 'Izdavanje') {
      return `${price} /mjes.`;
    } else {
      return price;
    }
  }
}
