import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten',
})
export class ShortenPipe implements PipeTransform {
  transform(value: string, shortenAt: number) {
    if (value.length > shortenAt) {
      return `${value.slice(0, shortenAt)}...`;
    } else {
      return value;
    }
  }
}
