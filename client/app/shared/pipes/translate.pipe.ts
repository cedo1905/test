import { Pipe, PipeTransform } from '@angular/core';
import { FiltersService } from 'app/main/real-estate/real-estate-list/filters/services/filter.service';

@Pipe({
  name: 'translate',
})
export class TranslatePipe implements PipeTransform {
  constructor(private filtersService: FiltersService) {}
  transform(name: string, filterName?: string) {
    let filter;
    switch (filterName) {
      case 'offerType':
        filter = this.filtersService.getOfferType();
        break;
      case 'residenceType':
        filter = this.filtersService.getResidenceType();
        break;
      case 'rooms':
        filter = this.filtersService.getRooms();
        break;
    }

    for (let value of filter) {
      if (name === value.key) {
        return value.name;
      }
    }
    return name;
  }
}
