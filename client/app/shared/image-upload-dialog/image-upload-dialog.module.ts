import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageUploadDialogComponent } from './image-upload-dialog.component';
import { ImageUploadModule } from '../image-upload/image-upload.module';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [ImageUploadDialogComponent],
  exports: [ImageUploadDialogComponent],
  imports: [CommonModule, ImageUploadModule, FormsModule, MatButtonModule, MatIconModule],
})
export class ImageUploadDialogModule {}
