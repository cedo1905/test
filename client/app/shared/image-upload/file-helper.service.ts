import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { SafeStyle, DomSanitizer } from '@angular/platform-browser';

export interface ImagePreview {
  width?: number;
  height?: number;
  name?: string;
  source: SafeStyle;
}

@Injectable({
  providedIn: 'root',
})
export class FileHelperService {
  constructor(private _httpClient: HttpClient, private sanitization: DomSanitizer) {}

  urlPathToFile(urlPath: string): Promise<File> {
    return this._httpClient
      .get(`${environment.mediaUrl}${urlPath}`, {
        responseType: 'arraybuffer',
      })
      .pipe(
        map((res: BlobPart) => {
          const file = new File([res], 'test.jpg');
          return file;
        })
      )
      .toPromise();
  }

  generateImageForUpload(file, image, wMax?, hMax?): ImagePreview {
    const elem = document.createElement('canvas');
    const ctx = elem.getContext('2d');
    const iw = image.width;
    const ih = image.height;
    let scale = 1;

    if (wMax || hMax) {
      scale = Math.min(wMax / iw, hMax / ih);
    }

    const iwScaled = iw * scale;
    const ihScaled = ih * scale;
    elem.width = iwScaled;
    elem.height = ihScaled;
    ctx.drawImage(image, 0, 0, iwScaled, ihScaled);
    const data = ctx.canvas.toDataURL();
    const sourceTrusted = this.sanitization.bypassSecurityTrustUrl(data);
    const imagePreviewObject: ImagePreview = {
      width: Math.round(iwScaled),
      height: Math.round(ihScaled),
      source: sourceTrusted,

      name: file.name,
    };
    return imagePreviewObject;
  }
}
