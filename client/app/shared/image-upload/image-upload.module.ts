import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { DropZoneDirective } from './drop-zone/drop-zone.directive';
import { RecipeImageUploadComponent } from './recipe-image-upload/recipe-image-upload.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [DropZoneDirective, RecipeImageUploadComponent],
  exports: [RecipeImageUploadComponent],
  imports: [CommonModule, MatIconModule, MatFormFieldModule, MatButtonModule],
})
export class ImageUploadModule {}
