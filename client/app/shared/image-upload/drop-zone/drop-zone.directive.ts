import { Directive, EventEmitter, HostListener, Output } from '@angular/core';

// https://angularfirebase.com/lessons/firebase-storage-with-angularfire-dropzone-file-uploader/
// https://github.com/AngularFirebase/82-angularfire-storage-dropzone/blob/master/src/app/drop-zone.directive.ts

@Directive({
  selector: '[dropZone]'
})
export class DropZoneDirective {
  @Output() dropped = new EventEmitter<FileList>();
  @Output() hovered = new EventEmitter<boolean>();

  @HostListener('drop', ['$event'])
  onDrop($event): void {
    $event.preventDefault();
    const fileList: FileList = $event.dataTransfer.files;
    if (fileList.length) {
      this.dropped.emit(fileList);
      this.hovered.emit(false);
    }
  }

  @HostListener('dragover', ['$event'])
  onDragOver($event): void {
    $event.preventDefault();
    this.hovered.emit(true);
  }

  @HostListener('dragleave', ['$event'])
  onDragLeave($event): void {
    $event.preventDefault();
    this.hovered.emit(false);
  }
}

