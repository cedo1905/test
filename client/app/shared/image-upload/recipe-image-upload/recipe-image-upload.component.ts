import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  OnInit,
  Self,
  ViewChild,
  Optional,
} from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { FileHelperService, ImagePreview } from '../file-helper.service';
import { environment } from 'environments/environment';

@Component({
  selector: 'recipe-image-upload',
  templateUrl: './recipe-image-upload.component.html',
  styleUrls: ['./recipe-image-upload.component.scss'],
})
export class RecipeImageUploadComponent implements OnInit, ControlValueAccessor {
  @Input() set allowedExtensions(extensions: string[]) {
    if (extensions) {
      this._allowedExtensions = extensions;
    }
  }

  imageUrl: any;

  get allowedExtensions(): string[] {
    return this._allowedExtensions;
  }

  @ViewChild('fileInput', { static: true }) fileInput;
  @Input() wMax: number;
  @Input() hMax: number;
  @Input() placeholder: string;
  @Input() label: string;
  @Input() multiple: boolean;
  @Input() isImage = true;
  @Output() fileChange = new EventEmitter<FileList>();

  isEmpty = true;
  private _allowedExtensions = [];

  file: File;
  files: File[] = [];
  isHovered = false;
  OnChangeFn: (value: any) => void;
  onTouchedFn: () => void;

  previewImage: ImagePreview = null;

  previewImages: ImagePreview[] = [];

  constructor(
    @Self() @Optional() public controlDir: NgControl,
    private sanitization: DomSanitizer,
    private _changeDetectorRef: ChangeDetectorRef,
    private fileHelperService: FileHelperService
  ) {
    controlDir.valueAccessor = this;
  }

  ngOnInit(): void {
    const control = this.controlDir.control;
    control.updateValueAndValidity();
  }

  writeValue(value: any): void {
    if (value) {
      this.previewImage = {
        source: environment.mediaUrl + value,
      };
    }
  }

  registerOnChange(fn: (value: any) => void): void {
    this.OnChangeFn = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouchedFn = fn;
  }

  setDisabledState(disabled: boolean): void {}

  setHoveredState(dragover: boolean): void {
    this.isHovered = dragover;
  }

  onFileInputChange(files: FileList): void {
    this.onTouchedFn();
    if (files && files.length) {
      this.file = files[0];
      this.OnChangeFn(this.file);

      this.handleImagePreview(this.file, (imagePreviewItem) => {
        this.previewImage = imagePreviewItem;
      });
    } else {
      this.previewImage = null;
      this.fileInput.nativeElement.value = null;
      this.OnChangeFn(null);
    }
  }

  handleImagePreview(file: File, setterFunction: (imagePreview: ImagePreview) => void): void {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      const source = e.target.result;
      const image = new Image();
      image.onload = () => {
        const imagePreviewObject = this.fileHelperService.generateImageForUpload(
          file,
          image,
          this.wMax,
          this.hMax
        );
        setterFunction(imagePreviewObject);
      };

      image.src = source;
    };

    reader.readAsDataURL(file);
  }
}
