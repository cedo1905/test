import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AuthGuard } from './auth/services/auth.guard';
import { RoleGuard } from './auth/services/role.guard';

const appRoutes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('app/auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: 'nekretnine',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./main/real-estate/real-estate.module').then((m) => m.RealEstateModule),
  },
  {
    path: 'korisnici',
    canActivate: [AuthGuard, RoleGuard],
    loadChildren: () =>
      import('./main/admin-panel/admin-panel.module').then((m) => m.AdminPanelModule),
  },
  {
    path: 'mediji',
    canActivate: [AuthGuard],
    loadChildren: () => import('./main/media/media.module').then((m) => m.MediaModule),
  },
  {
    path: '',
    redirectTo: '/nekretnine/list',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: '/nekretnine/list',
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  providers: [AuthGuard, RoleGuard],
  exports: [RouterModule],
})
export class AppRoutingModule {}
