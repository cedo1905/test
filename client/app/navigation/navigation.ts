import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
  {
    id: 'opcije',
    title: 'Opcije',
    type: 'group',
    children: [
      {
        id: 'real-estate',
        title: 'Nekretnine',
        type: 'item',
        icon: 'home',
        url: '/nekretnine',
      },
      {
        id: 'media',
        title: 'Mediji',
        type: 'item',
        icon: 'insert_photo',
        url: '/mediji',
      },
      {
        id: 'admin-panel',
        title: 'Admini',
        type: 'item',
        icon: 'perm_identity',
        url: '/korisnici',
      },
    ],
  },
];
