import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { RealEstateComponent } from './real-estate.component';
import { RealEstateListComponent } from './real-estate-list/real-estate-list.component';
import { RealEstateEditComponent } from './real-estate-edit/real-estate-edit.component';
import { RealEstateResolverService } from './services/real-estate.resolver';

const appRoutes: Routes = [
  {
    path: '',
    component: RealEstateComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: RealEstateListComponent,
      },
      {
        path: 'list/:state',
        component: RealEstateEditComponent,
        resolve: { propertyData: RealEstateResolverService },
      },
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule],
})
export class RealEstateRoutingModule {}
