import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Real-estate Modules/Components
import { RealEstateComponent } from './real-estate.component';
import { RealEstateRoutingModule } from './real-estate-routing.module';
import { RealEstateListModule } from './real-estate-list/real-estate-list.module';
import { RealEstateEditModule } from './real-estate-edit/real-estate-edit.module';

@NgModule({
  declarations: [RealEstateComponent],
  imports: [CommonModule, RealEstateRoutingModule, RealEstateListModule, RealEstateEditModule],
})
export class RealEstateModule {}
