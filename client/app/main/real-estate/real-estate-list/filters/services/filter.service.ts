import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { HttpParams } from '@angular/common/http';

export interface City {
  name: string;
  pos: {
    lat: number;
    lng: number;
  };
  settlement: string[];
}

@Injectable({ providedIn: 'root' })
export class FiltersService {
  resetFilter = new Subject<boolean>();
  private filters = new BehaviorSubject<HttpParams | null>(null);
  private sort = new BehaviorSubject<string | null>(null);

  filters$ = this.filters.asObservable();
  sort$ = this.sort.asObservable();

  residenceType: any = [
    {
      name: 'Stan',
      key: 'apartment',
    },
    {
      name: 'Kuća',
      key: 'house',
    },
    {
      name: 'Zemljište',
      key: 'land',
    },
    {
      name: 'Poslovni prostor',
      key: 'office',
    },
  ];

  rooms: any = [
    {
      name: 'Garsonjera',
      key: 'studio',
      active: false,
    },
    {
      name: 'Jednosoban',
      key: '1',
      active: false,
    },
    {
      name: 'Dvosoban',
      key: '2',
    },
    {
      name: 'Trosoban',
      key: '3',
    },
    {
      name: 'Četvorosoban',
      key: '4',
    },
    {
      name: 'Petosoban',
      key: '5',
    },
    {
      name: 'Ostalo',
      key: 'other',
    },
  ];

  offerType: any = [
    {
      name: 'Izdavanje',
      key: 'rent',
    },
    {
      name: 'Prodaja',
      key: 'sale',
    },
  ];

  activeRS: any = [
    {
      name: 'Aktivna',
      key: true,
    },
    {
      name: 'Neaktivna',
      key: false,
    },
  ];

  cities: City[] = [
    {
      name: 'Podgorica',
      pos: {
        lat: 42.44251,
        lng: 19.25733,
      },
      settlement: [
        'Centar',
        'Preko Morače',
        'Blok 5',
        'Blok 6',
        'Tološi',
        'Momišići',
        'Gornja Gorica',
        'Donja Gorica',
        'Gorica C',
        'Zagorič',
        'Zlatica',
        'Murtovina',
        'Masline',
        'Konik',
        'Vrela Ribnička',
        'Stari Aerodrom',
        'Pobrežje',
        'Čepurci',
        'Ljubović',
        'Drač (Podgorica)',
        'Dahna',
        'Stara Varoš',
        'Zabjelo',
        'Tuški put',
        'Dajbabe',
        'Donji Kokoti',
        'Vranići',
        'Blok 9',
        'City Kvart',
        'Međeđak',
        'Malo Brdo',
        'Sadine',
        'Kokoti',
        'Lekići',
        'Cvijetin Brijeg',
        'Pejton',
        'Rogami',
        'Ljubović',
      ],
    },
    {
      name: 'Nikšić',
      pos: {
        lat: 42.778557,
        lng: 18.94994,
      },
      settlement: [
        'Bistričko naselje',
        'Centar',
        'Grebice',
        'Klicevo',
        'Kocani',
        'Milocani',
        'Nikšićka zupa',
        'Pricelje',
        'Rastoci',
        'Rastovac',
        'Rubeža',
        'Rudo polje',
        'Sobaici',
        'Strasevina',
        'Velimlje',
        'Vidrovan',
      ],
    },
    {
      name: 'Budva',
      pos: {
        lat: 42.288692,
        lng: 18.842999,
      },
      settlement: [
        'Babin do',
        'Becici',
        'Centar',
        'Golubovina',
        'Gospostina',
        'Jaz',
        'Kod autobuske stanice',
        'Maine',
        'Mainski put',
        'Petrovac',
        'Ploce',
        'Pod Dubovicom',
        'Podmaine',
        'Prijevor',
        'Przno',
        'Radanovici',
        'Rijeka Rezevica',
        'Rozino',
        'Stanisici',
        'Sveti Stefan',
        'Velji vinogradi',
        'Zagora',
      ],
    },
    {
      name: 'Cetinje',
      pos: {
        lat: 42.395214,
        lng: 18.917063,
      },
      settlement: [
        'Bajice',
        'Centar',
        'Donje Polje',
        'Donji kraj',
        'Dujeva',
        'Gipos',
        'Gruda',
        'Humci',
        'Meterizi',
        'Nacionalni park Lovcen',
        'Njegusi',
        'Rijeka Crnojevica',
        'Rvasi',
      ],
    },
    {
      name: 'Bijelo Polje',
      pos: {
        lat: 43.034348,
        lng: 19.745554,
      },
      settlement: ['Jabučko', 'Lijeska'],
    },
    {
      name: 'Bar',
      pos: {
        lat: 42.096581,
        lng: 19.096286,
      },
      settlement: [
        'Bjelisi',
        'Buljarice',
        'Celuga',
        'Centar',
        'Dedići',
        'Dobre vode',
        'Ilino',
        'Makedonsko naselje',
        'Podgrad',
        'Polje',
        'Popovici',
        'Skadarsko jezero',
        'Susanj',
        'Utjeha',
        'Veliki pijesak',
        'Zeleni pojas',
      ],
    },
    {
      name: 'Herceg Novi',
      pos: {
        lat: 42.458023,
        lng: 18.523209,
      },
      settlement: [
        'Baošići',
        'Bijela',
        'Centar',
        'Igalo',
        'Kamanari',
        'Kumbor',
        'Savina',
        'Sutorina',
        'Zelenika',
        'Đenovići',
      ],
    },
    {
      name: 'Andrijevica',
      pos: {
        lat: 42.734493,
        lng: 19.787815,
      },
      settlement: ['Patkovica'],
    },
    {
      name: 'Berane',
      pos: {
        lat: 42.846912,
        lng: 19.868776,
      },
      settlement: ['Centar', 'Lokve'],
    },
    {
      name: 'Danilovgrad',
      pos: {
        lat: 42.551086,
        lng: 19.105139,
      },
      settlement: [
        'Bandici',
        'Bjelopavlici',
        'Brijestovo',
        'Centar',
        'Daljam',
        'Donji Zagarac',
        'Frutak',
        'Glavica',
        'Grbe',
        'Gruda',
        'Jastreb',
        'Klikovace',
        'Kosic',
        'Kosovi lug',
        'Lazine',
        'Martinici',
        'Novo selo',
        'Orja luka',
        'Pažići',
        'Pitoma loza',
        'Pričelje',
        'Slap Zete',
        'Sobaici',
        'Spuz',
        'Susica',
        'Tomasevici',
        'Zagarac',
        'Ćurilac',
      ],
    },
    {
      name: 'Gusinje',
      pos: {
        lat: 42.56348,
        lng: 19.832918,
      },
      settlement: ['Kruševo', 'Vusanje', 'Centar', 'Martinovići'],
    },
    {
      name: 'Kolašin',
      pos: {
        lat: 42.824055,
        lng: 19.52103,
      },
      settlement: [
        'Bare kraljske',
        'Centar',
        'Crkvine',
        'Lipovo',
        'Radigojno',
        'Smailagica polje',
        'Verusa',
      ],
    },
    {
      name: 'Kotor',
      pos: {
        lat: 42.424763,
        lng: 18.771154,
      },
      settlement: [
        'Centar',
        'Dobrota',
        'Krimovica',
        'Krivosije Gornje',
        'Lješevići',
        'Ljuta',
        'Muo',
        'Orahovac',
        'Perast',
        'Prčanj',
        'Radanovici',
        'Risan',
        'Skaljari',
        'Stari grad',
        'Stoliv',
        'Sveta Vraca',
        'Sveti Stasije',
      ],
    },
    {
      name: 'Mojkovac',
      pos: {
        lat: 42.96052,
        lng: 19.581821,
      },
      settlement: ['Centar'],
    },
    {
      name: 'Petnjica',
      pos: {
        lat: 42.908127,
        lng: 19.960226,
      },
      settlement: ['Koraći'],
    },
    {
      name: 'Plav',
      pos: {
        lat: 42.599766,
        lng: 19.940445,
      },
      settlement: ['Centar', 'Hoti'],
    },
    {
      name: 'Plužine',
      pos: {
        lat: 43.155067,
        lng: 18.842077,
      },
      settlement: ['Centar'],
    },
    {
      name: 'Pljevlja',
      pos: {
        lat: 43.356323,
        lng: 19.351988,
      },
      settlement: ['Kosanica', 'Zenica'],
    },
    {
      name: 'Rožaje',
      pos: {
        lat: 42.84219,
        lng: 20.167208,
      },
      settlement: ['Šusteri'],
    },
    {
      name: 'Šavnik',
      pos: {
        lat: 42.957397,
        lng: 19.095955,
      },
      settlement: ['Centar'],
    },
    {
      name: 'Tivat',
      pos: {
        lat: 42.4326,
        lng: 18.706423,
      },
      settlement: [
        'Bjelila',
        'Centar',
        'Donja Lastva',
        'Krašići',
        'Lepetani',
        'Mazina',
        'Mrčevac',
        'Radosevici',
        'Seljanovo',
      ],
    },
    {
      name: 'Tuzi',
      pos: {
        lat: 42.36852,
        lng: 19.317526,
      },
      settlement: ['Centar', 'Drume', 'Vuksanlekići', 'Vulaj', 'Šipčanik'],
    },
    {
      name: 'Ulcinj',
      pos: {
        lat: 41.929085,
        lng: 19.213608,
      },
      settlement: ['Centar', 'Pinjes', 'Stoj', 'Ulcinjsko polje'],
    },
    {
      name: 'Žabljak',
      pos: {
        lat: 43.155525,
        lng: 19.125652,
      },
      settlement: [
        'Borje',
        'Budecevica',
        'Centar',
        'Javorovača',
        'Kovacko polje',
        'Moticki gaj',
        'Nacionalni park',
        'Razvršje',
        'Savin kuk',
        'Tepacko polje',
        'Tmajevci',
      ],
    },
  ];

  getCities() {
    return this.cities.slice();
  }

  getResidenceType() {
    return this.residenceType.slice();
  }

  getRooms() {
    return this.rooms.slice();
  }

  getOfferType() {
    return this.offerType.slice();
  }

  getActiveRS() {
    return this.activeRS.slice();
  }

  getSettlement(selectedCity: City) {
    for (const city of this.cities) {
      if (selectedCity === city) {
        return city.settlement.slice();
      }
    }
  }

  filtersNext(value: HttpParams | null) {
    this.filters.next(value);
  }

  sortNext(value: string) {
    this.sort.next(value);
  }
}
