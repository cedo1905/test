import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';

//PrimeNG imports
import { MultiSelectModule } from 'primeng/multiselect';
import { AccordionModule } from 'primeng/accordion';
import { CheckboxModule } from 'primeng/checkbox';
import { InputNumberModule } from 'primeng/inputnumber';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { RealEstateSearchModule } from './real-estate-search/real-estate-search.module';

import { FiltersComponent } from '../filters/filters.component';
import { ButtonModule } from 'primeng/button';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [FiltersComponent],
  imports: [
    CommonModule,
    RealEstateSearchModule,
    SharedModule,

    //PrimeNG modules
    AccordionModule,
    MultiSelectModule,
    FormsModule,
    CheckboxModule,
    InputNumberModule,
    DropdownModule,
    RealEstateSearchModule,
    ButtonModule,
  ],
  exports: [FiltersComponent],
  providers: [CurrencyPipe],
})
export class FiltersModule {}
