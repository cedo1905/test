import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { FiltersService } from './services/filter.service';
import { RealEstateService } from '../../services/real-estate.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { CurrencyPipe } from '@angular/common';
import { Subscription } from 'rxjs';

interface City {
  name: string;
  pos: {
    lat: number;
    lng: number;
  };
  settlement: string[];
}

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
  animations: [fuseAnimations],
  host: {
    '(window:click)': 'onClick($event)',
  },
})
export class FiltersComponent implements OnInit, OnDestroy {
  getListSub: Subscription;
  sortSub: Subscription;
  filtersOpen = false;
  mapOpen = true;
  routeChange = false;
  toggleClass = false;

  //Price From To Input
  priceFrom: number;
  priceTo: number;
  priceInputTitle = 'Cijena';

  cities: City[];
  selectedCity: City;
  settlements: any;
  selectedSettlement: string;
  residenceType: FiltersService['residenceType'];
  selectedResidence: any;
  selectedOfferType: any;
  selectedActivity: any;
  checked: boolean = false;
  rooms: FiltersService['rooms'];
  offerType: FiltersService['offerType'];
  selectedRooms: FiltersService['rooms'];
  activity: FiltersService['activeRS'];

  constructor(
    private filtersService: FiltersService,
    private realEstateService: RealEstateService,
    private _fuseProgressBarService: FuseProgressBarService,
    private currencyPipe: CurrencyPipe
  ) {
    this.rooms = this.filtersService.getRooms();
    this.residenceType = this.filtersService.getResidenceType();

    this.cities = this.filtersService.getCities();
    this.cities.unshift({ name: 'Sve', pos: undefined, settlement: undefined });
    this.offerType = this.filtersService.getOfferType();
    this.offerType.unshift({ name: 'Sve', key: undefined });
    this.activity = this.filtersService.getActiveRS();
    this.activity.unshift({ name: 'Sve', key: undefined });
  }

  ngOnInit(): void {}

  onCity(selectedCity: City) {
    //Get districts from the selected city
    this.selectedSettlement = null;
    this.settlements = null;
    if (selectedCity.settlement !== undefined) {
      const cityVal = this.filtersService.getSettlement(selectedCity);
      if (cityVal.length !== 0) {
        cityVal.unshift('Sve');
      }
      this.settlements = cityVal?.map((item) => {
        return {
          name: item,
        };
      });
    }
  }

  //Toggle  dropdown Cijena
  onAddClass($event) {
    this.toggleClass = !this.toggleClass;
    $event.stopPropagation();
  }

  onClick(event) {
    if (!(event.target.matches('.dropdown-content') || event.target.matches('.price-fields'))) {
      this.toggleClass = false;
    }
  }
  onSelectResidence() {
    this.getFilteredData(
      this.selectedCity,
      this.selectedSettlement,
      this.selectedResidence?.map((el) => el.key),
      this.priceFrom,
      this.priceTo,
      this.selectedRooms?.map((room) => room.key),
      this.selectedOfferType,
      this.selectedActivity
    );
  }
  private getFilteredData(
    selectedCity?: any,
    district?: any,
    residenceType?: any,
    priceFrom?: any,
    priceTo?: any,
    rooms?: any,
    offerType?: any,
    activity?: any
  ) {
    //Pass params to realEstateService
    let params = new HttpParams();
    let sort: string;
    if (selectedCity && selectedCity !== '' && selectedCity.name !== 'Sve') {
      params = params.append('city', selectedCity.name);
    }
    if (district && district !== '' && district.name !== 'Sve') {
      params = params.append('district', district.name);
    }
    if (residenceType && residenceType.length !== 0) {
      params = params.append('residenceType', residenceType);
    }
    if (priceFrom && priceFrom !== '') {
      params = params.append('priceFrom', priceFrom);
    }
    if (priceTo && priceTo !== '') {
      params = params.append('priceTo', priceTo);
    }
    if (rooms && rooms.length !== 0) {
      params = params.append('rooms', rooms);
    }
    if (offerType && offerType !== '' && offerType.key) {
      params = params.append('offerType', offerType.key);
    }
    if (activity && activity !== '' && activity.key !== undefined) {
      params = params.append('active', activity.key);
    }

    this._fuseProgressBarService.show();
    this.filtersService.filtersNext(params);

    //Listening if stuff was sorted
    this.sortSub = this.filtersService.sort$.subscribe((sortData) => {
      sort = sortData;
    });

    // Get data with filters and sorting
    this.getListSub = this.realEstateService.getPropertyList(10, 0, params, sort).subscribe(
      (propertyList) => {
        this._fuseProgressBarService.hide();
      },
      (error) => {
        this._fuseProgressBarService.hide();
      }
    );
  }
  priceTitle(priceFrom?: number, priceTo?: number) {
    //Change price name to its values
    let priceTitleVal: string = 'Cijena';

    if (!!priceFrom) {
      priceTitleVal = `${this.currencyPipe.transform(priceFrom, 'EUR', 'symbol', '1.0-0')} +`;
    }
    if (!!priceTo) {
      priceTitleVal = `Do ${this.currencyPipe.transform(priceTo, 'EUR', 'symbol', '1.0-0')}`;
    }
    if (!!priceFrom && !!priceTo) {
      priceTitleVal = `${this.currencyPipe.transform(
        priceFrom,
        'EUR',
        'symbol',
        '1.0-0'
      )} - ${this.currencyPipe.transform(priceTo, 'EUR', 'symbol', '1.0-0')}`;
    }

    this.priceInputTitle = priceTitleVal;
  }

  resetFilters() {
    //Reset the filters and sorting
    this.selectedCity = null;
    this.selectedSettlement = null;
    this.selectedResidence = null;
    this.priceFrom = null;
    this.priceTo = null;
    this.selectedRooms = null;
    this.selectedOfferType = null;
    this.priceInputTitle = 'Cijena';
    this.selectedActivity = null;
    this.filtersService.resetFilter.next(true);
    this.settlements = null;
    this.getFilteredData(
      this.selectedCity,
      this.selectedSettlement,
      this.selectedResidence?.map((el) => el.key),
      this.priceFrom,
      this.priceTo,
      this.selectedRooms?.map((room) => room.key),
      this.selectedOfferType,
      this.selectedActivity
    );
  }

  resetFiltersNoReq() {
    this.selectedCity = null;
    this.selectedSettlement = null;
    this.selectedResidence = null;
    this.priceFrom = null;
    this.priceTo = null;
    this.selectedRooms = null;
    this.selectedOfferType = null;
    this.priceInputTitle = 'Cijena';
    this.selectedActivity = null;
    this.filtersService.resetFilter.next(true);
    this.filtersService.filtersNext(null);
    this.settlements = null;
  }

  ngOnDestroy() {
    this.resetFiltersNoReq();
    this.getListSub?.unsubscribe();
    this.sortSub?.unsubscribe();
  }
}
