import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Subscription } from 'rxjs';
import { PropertyModel } from '../../../../../../../shared/property.model';
import {
  RealEstateService,
  PropertyModelResponse,
} from 'app/main/real-estate/services/real-estate.service';

@Component({
  selector: 'app-real-estate-search',
  templateUrl: './real-estate-search.component.html',
  styleUrls: ['./real-estate-search.component.scss'],
  animations: [fuseAnimations],
})
export class RealEstateSearchComponent implements OnInit, OnDestroy {
  property: PropertyModel = null;
  subscription: Subscription;
  constructor(private realEstateService: RealEstateService) {}

  ngOnInit(): void {}

  searchForProperty(event) {
    const property = event.target.value.trim();
    if (property !== '') {
      this.subscription = this.realEstateService.searchProperty(property).subscribe(
        (propertyData: PropertyModelResponse) => {
          if (propertyData && propertyData.status === 'Success') {
            this.property = propertyData.data.property;
          }
        },
        (error) => {
          console.log(error);
          this.property = null;
        }
      );
    } else {
      this.property = null;
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
