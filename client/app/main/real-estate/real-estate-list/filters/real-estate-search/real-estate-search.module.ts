import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//MaterialModules
import { MatIconModule } from '@angular/material/icon';

//Real-estate Modules/Components
import { RealEstateSearchComponent } from './real-estate-search.component';
import { MatListModule } from '@angular/material/list';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { AddressCustomPipe } from 'app/shared/pipes/address.pipe';

@NgModule({
  declarations: [RealEstateSearchComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,

    //Material modules
    MatIconModule,
    MatListModule,
  ],
  exports: [RealEstateSearchComponent],
})
export class RealEstateSearchModule {}
