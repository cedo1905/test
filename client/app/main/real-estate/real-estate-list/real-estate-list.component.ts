import { Component, OnInit, ViewChild, OnDestroy, ɵConsole } from '@angular/core';
import { MatSort, MatSortable } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { fuseAnimations } from '@fuse/animations';
import { PropertyModel } from '../../../../../shared/property.model';
import { Subscription } from 'rxjs';
import { RealEstateService, PropertyModelResponse } from '../services/real-estate.service';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent } from 'app/shared/dialog-box/dialog-box.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { FiltersService } from './filters/services/filter.service';

@Component({
  selector: 'app-real-estate-list',
  templateUrl: './real-estate-list.component.html',
  styleUrls: ['./real-estate-list.component.scss'],
  animations: fuseAnimations,
})
export class RealEstateListComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  environment = environment;
  displayedColumns: string[] = [
    'id',
    'offerType',
    'residenceType',
    'rooms',
    'address',
    'area',
    'price',
    'creator',
    'isActive',
    'options',
  ];

  propertyList: PropertyModel[] = [];
  subscription: Subscription;
  getSub: Subscription;
  filterSub: Subscription;
  sortSub: Subscription;
  resetFilterSub: Subscription;

  //Waiting for Data
  loadingData = true;
  propertyGetError = null;

  //MatPaginator Inputs
  length = 0;
  pageSizeOptions: number[] = [5, 10, 25, 50];
  pageSize = 10;
  skipAmount = 0;
  dataSource = new MatTableDataSource();

  //filtersService
  filter: { name: string; key: PropertyModel['offerType'] }[];
  sorter: string;

  constructor(
    private realEstateService: RealEstateService,
    private filtersService: FiltersService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private _fuseProgressBarService: FuseProgressBarService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getPropertyData(this.pageSize, this.skipAmount);
    //list setup
    this.subscription = this.realEstateService.realEstateList$.subscribe(
      (propertyList: PropertyModelResponse) => {
        if (propertyList && propertyList.status === 'Success') {
          this._fuseProgressBarService.hide();
          this.propertyList = propertyList.data.property;

          this.dataSource = new MatTableDataSource(this.propertyList);
          this.length = propertyList.data.total;
          this.loadingData = false;
        }
      },
      (error) => {
        this._fuseProgressBarService.hide();
        this.loadingData = false;
        this.propertyGetError = error;
      }
    );
    this.dataSource.paginator = this.paginator;

    //Listen if filters were applied/removed
    this.filterSub = this.filtersService.filters$.subscribe((data) => {
      this.paginator?.firstPage();
    });

    //listen if data was sorted
    this.sortSub = this.filtersService.sort$.subscribe((data) => {
      this.paginator?.firstPage();
    });

    //listen for reset sort
    this.resetFilterSub = this.filtersService.resetFilter.subscribe((value) => {
      if (value === true) {
        this.sort.sort({ start: 'asc' } as MatSortable);
        this.dataSource.sort = this.sort;
        this.filtersService.sortNext(null);
      }
    });
  }

  paginatorEvent(pageEvent: PageEvent) {
    this.pageSize = pageEvent.pageSize;
    this.skipAmount = pageEvent.pageIndex * pageEvent.pageSize;
    this.getPropertyData(this.pageSize, this.skipAmount);
  }

  private getPropertyData(pageSize, skipAmount) {
    let paramsVal: HttpParams;
    let sortVal: string;

    //Apply filters if they exist
    this.filterSub = this.filtersService.filters$.subscribe((params) => {
      paramsVal = params;
    });

    //Apply sort if it exists
    this.sortSub = this.filtersService.sort$.subscribe((sortData) => {
      sortVal = sortData;
    });

    setTimeout(() => {
      this._fuseProgressBarService.show();
    });

    this.getSub = this.realEstateService
      .getPropertyList(pageSize, skipAmount, paramsVal, sortVal)
      .subscribe();
  }

  openDialog(propertyId: number) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      message: `Da li ste sigurni da zelite da izbrisete nekrentinu koja ima ID: "${propertyId}"?`,
    };

    const dialogRef = this.dialog.open(DialogBoxComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.deleteRealEstate(propertyId);
      }
    });
  }

  private deleteRealEstate(propertyId: number) {
    this.realEstateService.deleteProperty(propertyId).subscribe(
      (data) => {
        this.getPropertyData(this.pageSize, this.skipAmount);
        this.snackBar.open(`Nekretnina uspjesno izbrisana`, `OK`, {
          duration: 3000,
        });
      },
      (error) => {
        this.snackBar.open(`Doslo je do greske prilikom brisanja nekretnine`, `OK`, {
          duration: 3000,
        });
        this._fuseProgressBarService.hide();
      }
    );
  }

  onUpdate(propertyId: string) {
    this.router.navigate([propertyId], { relativeTo: this.route });
  }

  activateRealEstate(propertyId: number, isActive: boolean) {
    this.realEstateService.toggleActivateProperty(propertyId, isActive).subscribe(
      (resposne) => {
        this.getPropertyData(this.pageSize, this.skipAmount);
        this.snackBar.open(
          `Nekretnina uspjesno ${isActive ? 'deaktivirana' : 'aktivirana'}`,
          `OK`,
          {
            duration: 3000,
          }
        );
      },
      (error) => {
        this.snackBar.open(
          `Doslo je do greske prilikom ${isActive ? 'deaktiviranja' : 'aktiviranja'} nekretnine`,
          `OK`,
          {
            duration: 3000,
          }
        );
        this._fuseProgressBarService.hide();
      }
    );
  }

  sortData(event: MatSort) {
    //Sort the data. Descending '-value', acending 'value', default '' AND avoid the reset event
    if (event.active) {
      if (event.direction === 'desc') {
        event.active = `-${event.active}`;
      }
      if (event.direction !== '') {
        this.filtersService.sortNext(event.active);
      } else {
        this.filtersService.sortNext(null);
      }
      this.getPropertyData(this.pageSize, this.skipAmount);
    }
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.getSub?.unsubscribe();
    this.filterSub?.unsubscribe();
    this.sortSub?.unsubscribe();
    this.resetFilterSub?.unsubscribe();
    setTimeout(() => {
      this._fuseProgressBarService.hide();
    });
  }
}
