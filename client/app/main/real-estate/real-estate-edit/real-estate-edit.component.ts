import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PropertyModel } from '../../../../../shared/property.model';
import { City, FiltersService } from '../real-estate-list/filters/services/filter.service';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { ImageUploadDialogComponent } from 'app/shared/image-upload-dialog/image-upload-dialog.component';
import { MediaService, MediaResponse } from 'app/main/media/services/media.service';
import { MatDialog } from '@angular/material/dialog';
import { environment } from 'environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { PropertyModelResponse, RealEstateService } from '../services/real-estate.service';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { MapSettingsService } from '../services/mapSettings.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {} from 'googlemaps';
import { ErrorStateMatcherService } from '../services/error-state-matcher.service';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-real-estate-edit',
  templateUrl: './real-estate-edit.component.html',
  styleUrls: ['./real-estate-edit.component.scss'],
  animations: [fuseAnimations],
})
export class RealEstateEditComponent implements OnInit, AfterViewInit, OnDestroy {
  id: number;
  environment: any;
  title: string;

  //Initializing Property from filtersService
  offerType: { name: string; key: PropertyModel['offerType'] };
  residenceType: { name: string; key: PropertyModel['residenceType'] };
  rooms: { name: string; key: PropertyModel['rooms'] };
  activity: FiltersService['activeRS'];

  //form
  form: FormGroup;
  matcher = new ErrorStateMatcherService();
  propertyUpdate: PropertyModelResponse;
  cities: City[];
  city: City;
  districts: string[];
  photos: string[] = [];

  //Google Map
  @ViewChild('map') mapElement: any;
  map: google.maps.Map;
  marker: google.maps.Marker;
  markerSettings;

  constructor(
    private fb: FormBuilder,
    private filtersService: FiltersService,
    private _fuseProgressBarService: FuseProgressBarService,
    private mediaService: MediaService,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private realEstateService: RealEstateService,
    private router: Router,
    private mapSettingsService: MapSettingsService,
    private snackBar: MatSnackBar
  ) {
    this.environment = environment;
    this.id = parseInt(this.route.snapshot.params.state);
    if (!Number.isNaN(this.id)) {
      this.title = `ID: ${this.id}`;
    }

    //Form Init
    this.cities = this.filtersService.getCities();
    this.offerType = this.filtersService.getOfferType();
    this.residenceType = this.filtersService.getResidenceType();
    this.rooms = this.filtersService.getRooms();
    this.activity = this.filtersService.getActiveRS();

    this.markerSettings = this.mapSettingsService.markerSettings;
  }

  ngOnInit(): void {
    this.route.data.subscribe(
      (data: { propertyData: PropertyModelResponse }) => {
        this.propertyUpdate = data.propertyData;
        if (this.propertyUpdate && this.propertyUpdate.status === 'Success') {
          this.photos = this.propertyUpdate.data.property.photos;
          this.citySelected(this.propertyUpdate.data.property.city);
        }
        this.checkForm();
      },
      (error) => {
        this.router.navigate(['/nekretnine', 'list']);
      }
    );
  }

  ngAfterViewInit() {
    this.initMap();
  }

  citySelected(cityVal: string) {
    this.form?.controls.district.setValue(null);

    for (let city of this.cities) {
      if (city.name === cityVal) {
        this.city = city;
        break;
      }
    }
    this.districts = this.city.settlement;
    this.form?.controls.district.enable();
    this.form?.controls.address.enable();

    if (this.marker && this.form) {
      this.map.setZoom(11);
      this.map.panTo(this.city.pos);
      this.marker.setPosition(this.city.pos);
    }
  }

  openDialog() {
    const ref = this.dialog.open(ImageUploadDialogComponent, {
      data: {
        onUpload: (photo) => {
          this._fuseProgressBarService.show();

          this.mediaService.uploadMedia(photo).subscribe(
            (response: MediaResponse) => {
              if (response.data.media.photo.includes(`${environment.mediaUrl}/`)) {
                response.data.media.photo = response.data.media.photo.split(
                  `${environment.mediaUrl}/`
                )[1];
              }
              this.photos.push(response.data.media.photo);
              this._fuseProgressBarService.hide();
              this.form.patchValue({
                photos: this.photos,
              });
              ref.close();
            },
            (error) => {
              this._fuseProgressBarService.hide();
            }
          );
        },
      },
      maxWidth: '600px',
      maxHeight: '95vh',
      width: '95%',
      autoFocus: true,
      panelClass: 'dialog-panel-class',
    });
  }

  private checkForm() {
    let propertyData: any = {
      offerType: '',
      residenceType: '',
      rooms: '',
      price: '',
      area: '',
      floor: '',
      toilet: '',
      elevator: false,
      furnished: false,
      terrace: false,
      privateParking: false,
      attic: false,
      owner: '',
      internet: '',
      photos: this.photos,
      gallery360: '',
      city: '',
      district: '',
      address: '',
      locationDescription: '',
      longitude: '',
      latitude: '',
      ownerFullName: '',
      ownerEmail: '',
      ownerPhoneNumber: '',
      isActive: false,
    };

    if (this.propertyUpdate) {
      propertyData = this.propertyUpdate.data.property;
    }

    this.form = this.fb.group({
      offerType: [propertyData.offerType, Validators.required],
      residenceType: [propertyData.residenceType, Validators.required],
      rooms: [propertyData.rooms, Validators.required],
      price: [propertyData.price, Validators.required],
      area: [propertyData.area, Validators.required],
      floor: [propertyData.floor, Validators.required],
      toilet: [propertyData.toilet, Validators.required],
      elevator: [propertyData.elevator, Validators.required],
      furnished: [propertyData.furnished, Validators.required],
      terrace: [propertyData.terrace, Validators.required],
      privateParking: [propertyData.privateParking, Validators.required],
      internet: [propertyData.internet, Validators.required],
      owner: [propertyData.owner, Validators.required],
      attic: [propertyData.attic, Validators.required],
      photos: [this.photos, Validators.required],
      gallery360: [propertyData.gallery360],
      city: [propertyData.city, Validators.required],
      district: [propertyData.district],
      address: [propertyData.address, Validators.required],
      locationDescription: [propertyData.locationDescription],
      longitude: [propertyData.longitude, Validators.required],
      latitude: [propertyData.latitude, Validators.required],
      ownerFullName: [propertyData.ownerFullName],
      ownerEmail: [propertyData.ownerEmail, Validators.email],
      ownerPhoneNumber: [propertyData.ownerPhoneNumber],
      isActive: [propertyData.isActive],
    });
  }

  onSave() {
    this.form.patchValue({
      photos: this.photos,
    });
    if (this.propertyUpdate) {
      this.realEstateService.updatePropertyInfo(this.id, this.form.value).subscribe(
        (response) => {
          this.snackBarOpen(`Nekretnina '${this.id}' uspesno uredjena`);
          this.router.navigate(['/nekretnine', 'list']);
          this._fuseProgressBarService.hide();
        },
        (error) => {
          if (typeof error === 'string') {
            this.snackBarOpen(`${error}`);
          } else {
            this.snackBarOpen(`Doslo je do greske prilikom cuvanja`);
          }
          this._fuseProgressBarService.hide();
        }
      );
    } else {
      this.realEstateService.addProperty(this.form.value).subscribe(
        (response) => {
          this.snackBarOpen(`Nekretnina uspesno kreirana`);
          this.router.navigate(['/nekretnine', 'list']);
        },
        (error) => {
          this.snackBarOpen(`${error}`);
        }
      );
    }
  }

  deletePreview(index: number) {
    this.photos.splice(index, 1);
    this.form.patchValue({
      photos: this.photos,
    });
  }

  drop(event: CdkDragDrop<any>) {
    this.photos[event.previousContainer.data.index] = event.container.data.item;
    this.photos[event.container.data.index] = event.previousContainer.data.item;
    this.form.patchValue({
      photos: this.photos,
    });
  }

  private snackBarOpen(message: string) {
    this.snackBar.open(message, `OK`, {
      duration: 3000,
    });
    this._fuseProgressBarService.hide();
  }

  private initMap() {
    this.map = new google.maps.Map(
      this.mapElement.nativeElement,
      this.mapSettingsService.mapProperties
    );

    this.marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      icon: this.markerSettings.icons.location,
      animation: google.maps.Animation.DROP,
    });

    if (this.propertyUpdate && this.propertyUpdate.status === 'Success') {
      let lng = parseFloat(this.propertyUpdate.data.property.longitude);
      let lat = parseFloat(this.propertyUpdate.data.property.latitude);
      if (lng === NaN || lat === NaN) {
        lng = 0;
        lat = 0;
      }
      const markerPos = new google.maps.LatLng(lat, lng);

      setTimeout(() => {
        this.map.setZoom(14);
        this.map.panTo(markerPos);
        this.marker.setPosition(markerPos);
      });
    }

    const self = this;
    this.marker.addListener('position_changed', function (event) {
      self.form.patchValue({
        latitude: self.marker.getPosition().lat(),
        longitude: self.marker.getPosition().lng(),
      });
    });
  }

  ngOnDestroy() {
    setTimeout(()=>{
      this._fuseProgressBarService.hide();
    })
  }
}
