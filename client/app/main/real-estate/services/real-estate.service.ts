import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { PropertyModel } from '../../../../../shared/property.model';
import { catchError, map } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { Router } from '@angular/router';
export interface PropertyModelResponse {
  data: {
    property: PropertyModel[] & PropertyModel;
    total?: number;
  };
  status: string;
}

@Injectable({ providedIn: 'root' })
export class RealEstateService {
  private realEstateList = new Subject<PropertyModelResponse | null>();

  constructor(private http: HttpClient, private router: Router) {}

  realEstateList$ = this.realEstateList.asObservable();

  getPropertyList(pageSize: number, skipAmount: number, params?: HttpParams, sort?: string) {
    let paramsVal = new HttpParams()
      .set('take', pageSize.toString())
      .set('skip', skipAmount.toString());

    params?.keys().forEach((keyName) => {
      paramsVal = paramsVal.append(keyName, params?.get(keyName));
    });

    if (sort) {
      paramsVal = paramsVal.append('sort', sort);
    }

    return this.http
      .get<PropertyModelResponse>(`${environment.serverUrl}/property`, {
        params: paramsVal,
      })
      .pipe(
        catchError(this.handleError),
        map((propertyList) => {
          this.realEstateListNext(propertyList);
        })
      );
  }

  getProperty(propertyId: number) {
    return this.http
      .get<PropertyModelResponse>(`${environment.serverUrl}/property/${propertyId}`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return this.handleError(error);
        }),
        map((response) => {
          const responseData = {
            ...response.data.property,
            photos: response.data.property.photos ? response.data.property.photos : [],
          };
          response.data.property = responseData;
          return response;
        })
      );
  }

  searchProperty(propertyId: number) {
    return this.http
      .get<PropertyModelResponse>(`${environment.serverUrl}/property/${propertyId}/adminsearch`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return this.handleError(error);
        })
      );
  }

  toggleActivateProperty(propertyId: number, isActive: boolean) {
    let actInact = 'active';
    switch (isActive) {
      case true:
        actInact = 'inactive';
        break;
    }
    return this.http
      .put<PropertyModelResponse>(`${environment.serverUrl}/property/${propertyId}/${actInact}`, {})
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return this.handleError(error);
        })
      );
  }

  updatePropertyInfo(propertyId: number, property: PropertyModel) {
    return this.http
      .put(`${environment.serverUrl}/property/${propertyId}`, {
        ...property,
      })
      .pipe(catchError(this.handleError));
  }

  deleteProperty(propertyId: number) {
    return this.http.delete(`${environment.serverUrl}/property/${propertyId}`);
  }

  addProperty(property: PropertyModel) {
    return this.http
      .post<PropertyModel>(`${environment.serverUrl}/property/add`, {
        ...property,
      })
      .pipe(catchError(this.handleError));
  }

  private handleError(errorMsg: HttpErrorResponse) {
    let errorMessage = 'An unknown error has occured!';
    if (!errorMsg.error) {
      return throwError(errorMessage);
    }
    if (errorMsg.status === 400 || errorMsg.status === 404) {
      this.router.navigate(['/nekretnine', 'list']);
      errorMessage = errorMsg.error.message;
      return throwError(errorMessage);
    }
    errorMessage = errorMsg.error.message;
    return throwError(errorMessage);
  }

  realEstateListNext(value: PropertyModelResponse | null) {
    this.realEstateList.next(value);
  }
}
