import { Injectable } from '@angular/core';
import { RealEstateService, PropertyModelResponse } from './real-estate.service';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class RealEstateResolverService implements Resolve<PropertyModelResponse> {
  constructor(private realEstateService: RealEstateService) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): PropertyModelResponse | Observable<PropertyModelResponse> | Promise<PropertyModelResponse> {
    const realEstateRoute = route.paramMap.get('state');

    if (realEstateRoute !== 'nova') {
      return this.realEstateService.getProperty(+realEstateRoute);
    } else {
      return null;
    }
  }
}
