import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class MapSettingsService {
  MONTENEGRO_BOUNDS = {
    north: 43.578024,
    south: 41.806486,
    west: 18.105382,
    east: 20.525677,
  };

  mapStyles = [
    {
      featureType: 'all',
      stylers: [
        {
          saturation: 0,
        },
        {
          hue: '#e7ecf0',
        },
      ],
    },
    {
      featureType: 'administrative.country',
      elementType: 'all',
      stylers: [
        {
          visibility: 'on',
        },
        {
          weight: '4',
        },
        {
          color: '#039be5',
        },
        {
          lightness: '0',
        },
        {
          saturation: '0',
        },
        {
          gamma: '1',
        },
      ],
    },
    {
      featureType: 'poi',
      elementType: 'all',
      stylers: [
        {
          visibility: 'off',
        },
      ],
    },
    {
      featureType: 'road',
      elementType: 'labels.icon',
      stylers: [
        {
          saturation: -70,
        },
      ],
    },
    {
      featureType: 'transit',
      stylers: [
        {
          visibility: 'off',
        },
      ],
    },
  ];

  mapProperties: any = {
    mapTypeId: 'roadmap',
    zoom: 7,
    maxZoom: 18,
    minZoom: 8,
    streetViewControl: false,
    center: { lat: 42.6695814, lng: 19.4874586 },
    restriction: {
      latLngBounds: this.MONTENEGRO_BOUNDS,
      strictBounds: false,
    },
    styles: this.mapStyles,
    zoomControl: true,
    panControl: true,
  };

  markerSettings: any = {
    icons: {
      location: {
        url: `assets/images/map-icon/location.svg`,
        size: new google.maps.Size(28, 35),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(14, 35),
        scaledSize: new google.maps.Size(28, 35),
      },
    },
  };
}
