import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'environments/environment';

export interface MediaResponse {
  status: string;
  data: {
    media: { _id: string; photo: string }[] & { _id: string; photo: string };
    total?: number;
  };
}
@Injectable({ providedIn: 'root' })
export class MediaService {
  private media = new BehaviorSubject<MediaResponse | null>(null);
  constructor(private http: HttpClient) {}

  media$ = this.media.asObservable();

  getAllMedia(pageSize: number, skipAmount: number) {
    return this.http
      .get<MediaResponse>(`${environment.serverUrl}/media`, {
        params: new HttpParams()
          .set('take', pageSize.toString())
          .set('skip', skipAmount.toString()),
      })
      .pipe(
        catchError(this.handleError),
        map((response) => {
          this.mediaNext(response);
        })
      );
  }

  uploadMedia(data) {
    const formData = new FormData();
    formData.set('photo', data);
    return this.http.post(`${environment.serverUrl}/media`, formData);
  }

  deleteMedia(mediaId: string) {
    return this.http.delete(`${environment.serverUrl}/media/${mediaId}`);
  }

  private handleError(errorMsg: HttpErrorResponse) {
    let errorMessage = 'An unknown error has occured!';
    if (!errorMsg.error) {
      return throwError(errorMessage);
    }
    errorMessage = errorMsg.error.message;
    return throwError(errorMessage);
  }

  mediaNext(value: MediaResponse) {
    this.media.next(value);
  }
}
