import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MediaRoutingModule } from './media-routing.module';
import { MediaComponent } from './media.component';
import { MediaListModule } from './media-list/media-list.module';

@NgModule({
  declarations: [MediaComponent],
  imports: [CommonModule, MediaRoutingModule, MediaListModule],
  exports: [],
})
export class MediaModule {}
