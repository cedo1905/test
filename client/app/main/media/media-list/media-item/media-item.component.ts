import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { environment } from 'environments/environment';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent } from 'app/shared/dialog-box/dialog-box.component';

@Component({
  selector: 'app-media-item',
  templateUrl: './media-item.component.html',
  styleUrls: ['./media-item.component.scss'],
})
export class MediaItemComponent implements OnInit {
  @Input() image: { _id: string; photo: string };
  @Output() deleted: EventEmitter<string> = new EventEmitter();

  environment: any;
  constructor(private dialog: MatDialog) {}

  ngOnInit(): void {
    this.environment = environment;
    if (this.image.photo.includes(`${environment.mediaUrl}/`)) {
      this.image.photo = this.image.photo.split(`${environment.mediaUrl}/`)[1];
    }
  }

  openDialog(imageId: string, image: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      message: `Da li ste sigurni da zelite da izbrisete sliku koja ima ID: ${imageId}?`,
      image: image,
    };

    const dialogRef = this.dialog.open(DialogBoxComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.deleteMedia(imageId);
      }
    });
  }

  private deleteMedia(mediaId: string) {
    this.deleted.emit(mediaId);
  }
}
