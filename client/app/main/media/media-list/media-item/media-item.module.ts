import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MediaItemComponent } from './media-item.component';

@NgModule({
  declarations: [MediaItemComponent],
  imports: [CommonModule, MatIconModule, MatProgressSpinnerModule],
  exports: [MediaItemComponent],
})
export class MediaItemModule {}
