import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MediaService, MediaResponse } from '../services/media.service';
import { Subscription } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { ImageUploadDialogComponent } from 'app/shared/image-upload-dialog/image-upload-dialog.component';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';

import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-media-list',
  templateUrl: './media-list.component.html',
  styleUrls: ['./media-list.component.scss'],
  animations: [fuseAnimations],
})
export class MediaListComponent implements OnInit, OnDestroy {
  loadingData = true;
  mediaList: { _id: string; photo: string }[] = [];
  subscrition: Subscription;
  getSub: Subscription;
  mediaGetError: string;

  //pagination
  length: number;
  pageSizeOptions = [24, 48, 96, 192];
  pageSize = this.pageSizeOptions[0];
  skipAmount = 0;
  constructor(
    private mediaService: MediaService,
    private dialog: MatDialog,
    private _fuseProgressBarService: FuseProgressBarService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.getMedia(this.pageSize, this.skipAmount);
    this.subscrition = this.mediaService.media$.subscribe(
      (media: MediaResponse) => {
        if (media && media.status === 'Success') {
          this.mediaList = media.data.media;
          this.length = media.data.total;
          this.loadingData = false;
          setTimeout(() => {
            this._fuseProgressBarService.hide();
          });
        }
      },
      (error) => {
        this.mediaGetError = error;
        this.loadingData = false;
        setTimeout(() => {
          this._fuseProgressBarService.hide();
        });
      }
    );
  }

  paginatorEvent(pageEvent: PageEvent) {
    this.pageSize = pageEvent.pageSize;
    this.skipAmount = pageEvent.pageIndex * pageEvent.pageSize;
    this.getMedia(this.pageSize, this.skipAmount);
  }

  startDialog() {
    const ref = this.dialog.open(ImageUploadDialogComponent, {
      data: {
        onUpload: (photo) => {
          this._fuseProgressBarService.show();

          this.mediaService.uploadMedia(photo).subscribe(
            (response: any) => {
              this.getMedia(this.pageSize, 0);
              ref.close();
            },
            (error) => {
              this._fuseProgressBarService.hide();
            }
          );
        },
      },
      maxWidth: '600px',
      maxHeight: '95vh',
      width: '95%',
      autoFocus: true,
      panelClass: 'dialog-panel-class',
    });
  }

  onDeleteMedia(mediaId: string) {
    this.mediaService.deleteMedia(mediaId).subscribe(
      (response) => {
        this.snackBar.open(`Slika uspjesno izbrisana`, `OK`, {
          duration: 3000,
        });
        this.getMedia(this.pageSize, this.skipAmount);
      },
      (error) => {
        this.snackBar.open(`Doslo je do greske prilikom brisanja slike`, `OK`, {
          duration: 3000,
        });
        this._fuseProgressBarService.hide();
      }
    );
  }

  private getMedia(pageSize: number, skipAmount: number) {
    setTimeout(() => {
      this._fuseProgressBarService.show();
    });
    this.getSub = this.mediaService.getAllMedia(pageSize, skipAmount).subscribe();
  }

  ngOnDestroy() {
    this.subscrition?.unsubscribe();
    this.getSub.unsubscribe();

    setTimeout(() => {
      this._fuseProgressBarService.hide();
    });
  }
}
