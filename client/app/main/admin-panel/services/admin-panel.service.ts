import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { throwError, BehaviorSubject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { UserModel } from '../../../../../shared/user.model';
import { Router } from '@angular/router';

export interface AdminPanelResponse {
  status: string;
  data: { user?: UserModel; users?: UserModel[]; total?: number };
}

@Injectable({ providedIn: 'root' })
export class AdminPanelService {
  private userList = new BehaviorSubject<AdminPanelResponse | null>(null);

  constructor(private http: HttpClient, private router: Router) {}

  userList$ = this.userList.asObservable();

  registerUser(form: {
    fullName: string;
    email: string;
    password: string;
    passwordConfirm: string;
    role: string;
  }) {
    return this.http
      .post<UserModel>(`${environment.serverUrl}/auth/register`, {
        fullName: form.fullName,
        email: form.email,
        password: form.password,
        passwordConfirm: form.passwordConfirm,
        role: form.role,
      })
      .pipe(catchError(this.handleError));
  }

  deleteUser(id: string) {
    return this.http.delete(`${environment.serverUrl}/user/${id}`);
  }

  getUserList(pageSize: number, skipAmount: number) {
    return this.http
      .get<AdminPanelResponse>(`${environment.serverUrl}/user`, {
        params: new HttpParams()
          .set('take', pageSize.toString())
          .set('skip', skipAmount.toString()),
      })
      .pipe(
        catchError(this.handleError),
        map((userList) => {
          this.userListNext(userList);
        })
      );
  }

  getUser(userId: string) {
    return this.http
      .get<AdminPanelResponse>(`${environment.serverUrl}/user/${userId}`)
      .pipe(catchError(this.handleError));
  }

  updateUserPassword(userId: string, form: { password: string; passwordConfirm: string }) {
    return this.http
      .put(`${environment.serverUrl}/user/${userId}/password`, {
        password: form.password,
        passwordConfirm: form.passwordConfirm,
      })
      .pipe(catchError(this.handleError));
  }

  updateUserInfo(userId: string, form: { fullName: string; email: string; role: string }) {
    return this.http
      .put(`${environment.serverUrl}/user/${userId}`, {
        fullName: form.fullName,
        email: form.email,
        role: form.role,
      })
      .pipe(catchError(this.handleError));
  }

  toggleBLockUser(userId: string, isActive: boolean) {
    let isBlocked: string;
    switch (isActive) {
      case true:
        isBlocked = 'block';
        break;
      default:
        isBlocked = 'unblock';
        break;
    }
    return this.http
      .put(`${environment.serverUrl}/user/${userId}/${isBlocked}`, {})
      .pipe(catchError(this.handleError));
  }
  private handleError(errorMsg: HttpErrorResponse) {
    let errorMessage = 'Doslo je do greske';
    if (!errorMsg.error) {
      return throwError(errorMessage);
    }
    errorMessage = errorMsg.error.message;
    return throwError(errorMessage);
  }

  userListNext(value: AdminPanelResponse | null) {
    this.userList.next(value);
  }
}
