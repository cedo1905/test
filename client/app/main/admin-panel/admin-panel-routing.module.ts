import { Routes, RouterModule } from '@angular/router';
import { AdminPanelComponent } from './admin-panel.component';
import { AdminPanelListComponent } from './admin-panel-list/admin-panel-list.component';
import { AdminEditComponent } from './admin-edit/admin-edit.component';
import { NgModule } from '@angular/core';

const appRoutes: Routes = [
  {
    path: '',
    component: AdminPanelComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: AdminPanelListComponent,
      },
      {
        path: 'list/novi',
        component: AdminEditComponent,
      },
      {
        path: 'list/promeni-lozinku/:state',
        component: AdminEditComponent,
      },
      {
        path: 'list/:state',
        component: AdminEditComponent,
      },
    ],
  },
];
@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule],
})
export class AdminPanelRoutingModule {}
