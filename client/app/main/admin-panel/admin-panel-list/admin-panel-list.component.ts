import { Component, OnInit, ViewChild, OnDestroy, HostListener } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AdminPanelService, AdminPanelResponse } from '../services/admin-panel.service';
import { UserModel } from '../../../../../shared/user.model';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogBoxComponent } from 'app/shared/dialog-box/dialog-box.component';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { response } from 'express';

@Component({
  selector: 'app-admin-panel-list',
  templateUrl: './admin-panel-list.component.html',
  styleUrls: ['./admin-panel-list.component.scss'],
  animations: [fuseAnimations],
})
export class AdminPanelListComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['fullName', 'email', 'isActive', 'options'];
  userList: UserModel[] = [];
  subscription: Subscription;
  getSub: Subscription;

  //Waiting for Data
  loadingData = false;
  userGetError = null;

  //MatPaginator Inputs
  length = 0;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageSize = this.pageSizeOptions[0];
  skipAmount = 0;
  dataSource = new MatTableDataSource();

  constructor(
    private adminPanelService: AdminPanelService,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private _fuseProgressBarService: FuseProgressBarService,
    private route: ActivatedRoute
  ) {}

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.loadingData = true;
    this.getUserData(this.pageSize, this.skipAmount);
    this.subscription = this.adminPanelService.userList$.subscribe(
      (userList: AdminPanelResponse) => {
        if (userList && userList.status === 'Success') {
          setTimeout(() => {
            this._fuseProgressBarService.hide();
          });
          this.userList = userList.data.users;
          this.dataSource = new MatTableDataSource(this.userList);
          this.length = userList.data.total;
          this.loadingData = false;
        }
      },
      (error) => {
        setTimeout(() => {
          this._fuseProgressBarService.hide();
        });
        this.loadingData = false;
        this.userGetError = error;
      }
    );
    this.dataSource.paginator = this.paginator;
  }

  paginatorEvent(pageEvent: PageEvent) {
    this.pageSize = pageEvent.pageSize;
    this.skipAmount = pageEvent.pageIndex * pageEvent.pageSize;
    this.getUserData(this.pageSize, this.skipAmount);
  }

  private getUserData(pageSize: number, skipAmount: number) {
    setTimeout(() => {
      this._fuseProgressBarService.show();
    });
    this.getSub = this.adminPanelService.getUserList(pageSize, skipAmount).subscribe();
  }

  private onUserDelete(userId: string) {
    // const dialogRef = this.dialog.open('aaaa', {});
    this.loadingData = true;
    this.adminPanelService.deleteUser(userId).subscribe(
      (resData) => {
        this.getUserData(this.pageSize, this.skipAmount);
        this.snackBar.open(`Profil uspjesno izbrisan`, `OK`, {
          duration: 3000,
        });
      },
      (error) => {
        this.snackBar.open(`Doslo je do greske prilikom brisanja profila`, `OK`, {
          duration: 3000,
        });
        this.loadingData = false;
      }
    );
  }

  openDialog(userId: string, userName: string) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      message: `Da li ste sigurni da zelite da izbrisete admina "${userName}"?`,
    };

    const dialogRef = this.dialog.open(DialogBoxComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.onUserDelete(userId);
      }
    });
  }
  onEditAdmin(userId: string) {
    this.router.navigate([userId], { relativeTo: this.route });
  }
  onChangePassword(userId: string) {
    this.router.navigate(['promeni-lozinku', userId], { relativeTo: this.route });
  }
  onCreate() {
    this.router.navigate(['novi'], { relativeTo: this.route });
  }
  blockUser(userId: string, isActive: boolean) {
    this.adminPanelService.toggleBLockUser(userId, isActive).subscribe(
      (response) => {
        this.snackBar.open(`Korisnik uspesno ${isActive ? 'blokiran' : 'odblokiran'}`, `OK`, {
          duration: 3000,
        });
        this.getUserData(this.pageSize, this.skipAmount);
      },
      (error) => {
        this.snackBar.open(`${error}`, `OK`, {
          duration: 3000,
        });
      }
    );
  }
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.getSub) {
      this.getSub.unsubscribe();
    }
    setTimeout(() => {
      this._fuseProgressBarService.hide();
    });
  }
}
