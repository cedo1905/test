import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Admin-panel Modules
import { AdminPanelRoutingModule } from './admin-panel-routing.module';
import { AdminPanelComponent } from './admin-panel.component';
import { AdminEditModule } from './admin-edit/admin-edit.module';
import { AdminPanelListModule } from './admin-panel-list/admin-panel-list.module';

@NgModule({
  declarations: [AdminPanelComponent],
  imports: [CommonModule, AdminPanelRoutingModule, AdminEditModule, AdminPanelListModule],
})
export class AdminPanelModule {}
