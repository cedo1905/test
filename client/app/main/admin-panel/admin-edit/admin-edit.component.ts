import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params, NavigationEnd, RouterEvent } from '@angular/router';
import { Subscription } from 'rxjs';
import { AdminPanelService, AdminPanelResponse } from '../services/admin-panel.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';
import { UserModel } from '../../../../../shared/user.model';

@Component({
  selector: 'app-admin-edit',
  templateUrl: './admin-edit.component.html',
  styleUrls: ['./admin-edit.component.scss'],
  animations: [fuseAnimations],
})
export class AdminEditComponent implements OnInit, OnDestroy {
  //form
  form: FormGroup;
  user: UserModel;
  index: string;
  isLoading = true;
  routeTitle: string;

  //routes
  createMode = 'list/novi';
  editMode = 'list/:state';
  passwordMode = 'list/promeni-lozinku/:state';

  //mode check
  mode: string;

  //subs
  navigationSubscription: Subscription;
  routeSub: Subscription;
  getSub: Subscription;

  /**
   * Constructor
   *
   * @param {FormBuilder} _formBuilder
   */

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private adminPanelService: AdminPanelService,
    private snackBar: MatSnackBar,
    private _fuseProgressBarService: FuseProgressBarService
  ) {
    //On every route event check mode, get the index, set up the form
    this.navigationSubscription = this.router.events.subscribe((e: RouterEvent) => {
      if (e instanceof NavigationEnd) {
        this.mode = this.route.snapshot.routeConfig.path;
        this.routeSub = this.route.params.subscribe((params: Params) => {
          this.index = params['state'];
        });
        this.checkForm();
      }
    });
  }

  ngOnInit(): void {}

  private checkForm() {
    let name = '';
    let email = '';
    let password = '';
    let passConf = '';
    let roleVal = 'admin';

    if (this.mode !== this.createMode) {
      this._fuseProgressBarService.show();
      this.getSub = this.adminPanelService.getUser(this.index).subscribe(
        (user: AdminPanelResponse) => {
          if (user && user.status === 'Success') {
            setTimeout(() => {
              this._fuseProgressBarService.hide();
            });

            //Apply User Data To Form
            this.user = user.data.user;
            name = this.user.fullName;
            email = this.user.email;
            roleVal = this.user.role;

            if (this.mode === this.editMode) {
              this.form.patchValue({
                fullName: name,
                email: email,
                role: roleVal,
              });
              this.routeTitle = `Uredi profil ${name}`;
            } else {
              this.routeTitle = `Promjena lozinke korisnika ${name}`;
            }
            this.isLoading = false;
          }
        },
        (error) => {
          this.router.navigate(['/korisnici', 'list']);
        }
      );
    } else {
      this.routeTitle = `Novi Admin`;
      this.isLoading = false;
    }

    //send data to form
    this.form = this.fb.group(
      {
        fullName: [{ value: name, disabled: this.mode === this.passwordMode }, Validators.required],
        email: [
          { value: email, disabled: this.mode === this.passwordMode },
          [Validators.required, Validators.email],
        ],
        role: [
          { value: roleVal, disabled: this.mode === this.passwordMode },
          [Validators.required],
        ],
        password: [
          { value: password, disabled: this.mode === this.editMode },
          [Validators.required, Validators.minLength(6)],
        ],
        passwordConfirm: [
          { value: passConf, disabled: this.mode === this.editMode },
          [Validators.required, Validators.minLength(6)],
        ],
      },
      { validator: this.MustMatch('password', 'passwordConfirm') }
    );
  }

  saveData() {
    this._fuseProgressBarService.show();
    this.adminPanelService.userListNext(null);
    switch (this.mode) {
      case this.editMode:
        this.adminPanelService.updateUserInfo(this.index, this.form.value).subscribe(
          (data) => {
            this.snackBarOpen(`Profil \"${this.form.value.fullName}\" uspjesno uredjen.`);
            this.router.navigate(['/korisnici', 'list']);
          },
          (error) => {
            this.snackBarOpen(`Doslo je do greske prilikom uredjivanja.`);
          }
        );
        break;
      case this.createMode:
        this.adminPanelService.registerUser(this.form.value).subscribe(
          (data) => {
            this.snackBarOpen(`Profil \"${this.form.value.fullName}\" uspjesno registrovan.`);
            this.router.navigate(['/korisnici', 'list']);
          },
          (error) => {
            this.snackBarOpen(`Doslo je do greske prilikom registrovanja!`);
          }
        );
        break;
      case this.passwordMode:
        this.adminPanelService.updateUserPassword(this.index, this.form.value).subscribe(
          (data) => {
            this.snackBarOpen(`Lozinka profila \"${this.user.fullName}\" uspjesno promenjena`);
            this.router.navigate(['/korisnici', 'list']);
          },
          (error) => {
            this.snackBarOpen('Doslo je do greske prilikom promjene lozinke.');
          }
        );
        break;
    }
  }

  private MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  private snackBarOpen(message: string) {
    this.snackBar.open(message, `OK`, {
      duration: 3000,
    });
    setTimeout(() => {
      this._fuseProgressBarService.hide();
    });
  }

  ngOnDestroy(): void {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }

    if (this.getSub) {
      this.getSub.unsubscribe();
    }

    setTimeout(() => {
      this._fuseProgressBarService.hide();
    });
  }
}
