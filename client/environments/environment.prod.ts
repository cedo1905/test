
export const environment = {
  production: true,
  serverUrl: 'https://nest360.me/api',
  mediaUrl: 'https://nest360.s3.amazonaws.com',
  clientUrl: 'https://nest360.me',
  webMailUrl: 'https://webmail.nest360.me'
};
